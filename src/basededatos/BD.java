package basededatos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class BD {

	private static BD instance;
	private Connection connection;
	private String url = "jdbc:postgresql://localhost:5432/baseCine";
	private String username = "postgres";
	private String password = "poo";

	private BD() {
		try {
			Class.forName("org.postgresql.Driver");
			try {
				this.connection = DriverManager.getConnection(url, username, password);
				System.out.println("Conexi�n correcta con el Driver Postgre");
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Problemas con la base de datos", "Sistema",
						JOptionPane.ERROR_MESSAGE);

			}
		} catch (ClassNotFoundException ex) {

			JOptionPane.showMessageDialog(null, "Problemas con la base de datos", "Sistema", JOptionPane.ERROR_MESSAGE);

		}
	}

	public Connection getConnection() {
		return connection;
	}

	public static BD getInstance() {
		if (instance == null) {
			instance = new BD();
		} else
			try {
				if (instance.getConnection().isClosed()) {
					instance = new BD();
				}
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Problemas con la base de datos", "Sistema", JOptionPane.ERROR_MESSAGE);
			}

		return instance;
	}

	public ResultSet listarEntidades(String consulta) {
		ResultSet rs = null;
		try {
			Statement s = connection.createStatement();

			rs = s.executeQuery(consulta);
		} catch (Exception e) {
		
			JOptionPane.showMessageDialog(null, "Problemas con la base de datos", "Sistema", JOptionPane.ERROR_MESSAGE);
		}
		return rs;
	}

	public Boolean manipularEntidades(String consulta, ArrayList<Object> parametros) {
		boolean retorno = false;
		try {
			PreparedStatement s = connection.prepareStatement(consulta);
			int i = 0;
			while (i < parametros.size()) {

				if (parametros.get(i) instanceof Integer) {

					s.setInt(i + 1, (int) parametros.get(i));
					i++;
				} else {

					s.setString(i + 1, (String) parametros.get(i));

					i++;
				}
			}
			s.executeUpdate();
			retorno = true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Problemas con la base de datos", "Sistema", JOptionPane.ERROR_MESSAGE);

		}
		return retorno;
	}


}
