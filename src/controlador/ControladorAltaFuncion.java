package controlador;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import modelo.Asiento;
import modelo.AsientoDaoImp;
import modelo.Funcion;
import modelo.FuncionDaoImpl;
import vista.FuncionAlta;

public class ControladorAltaFuncion implements ActionListener, KeyListener, WindowListener, ItemListener {

	private FuncionAlta vistaAltaFuncion;
	private FuncionDaoImpl fdi = new FuncionDaoImpl();
	private AsientoDaoImp adi = new AsientoDaoImp();

	public ControladorAltaFuncion() {

		this.setVistaAltaFuncion(new FuncionAlta(this));

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getVistaAltaFuncion().getBtnGuardar()) {
			Integer codigo_pelicula = null;
			Integer codigo_sala = null;
			String nombre_funcion = this.getVistaAltaFuncion().getTextFieldNombreFuncion().getText();
			String pelicula = this.getVistaAltaFuncion().getCmboBxPelicula().getSelectedItem().toString();
			String sala = this.getVistaAltaFuncion().getCmboBxSala().getSelectedItem().toString();

			String formato = this.getVistaAltaFuncion().getDateChooser().getDateFormatString();
			Date fdate = this.getVistaAltaFuncion().getDateChooser().getDate();
			SimpleDateFormat sdf = new SimpleDateFormat(formato);

			String fechaFuncion = String.valueOf(sdf.format(fdate));
			String horario = this.setearHora(this.getVistaAltaFuncion().getSpinerHora().getValue().toString()) + ":"
					+ this.setearHora(this.getVistaAltaFuncion().getSpinerMin().getValue().toString());
			String precio = this.getVistaAltaFuncion().getTxtFieldPrecio().getText();

			Boolean validacion = (!precio.equals("") && !nombre_funcion.equals(""));

			if (validacion) {
				codigo_pelicula = this.obtenerCodigo(pelicula);
				codigo_sala = this.obtenerCodigo(sala);
				String descuento;
				if (this.descuento(fdate).equals("Jueves") || this.descuento(fdate).equals("Martes")) {
					Double d = Double.parseDouble(precio);
					d = d * 0.25;
					descuento = String.valueOf(d);
				} else {
					descuento = "0";
				}

				Funcion funcion = new Funcion(codigo_pelicula, nombre_funcion, String.valueOf(codigo_sala),
						fechaFuncion, horario, precio, descuento);
				if (this.getFdi().agregarFuncion(funcion)) {

					Funcion f2 = this.getFdi().buscarUltima();
					List<Asiento> a = this.getAdi().buscarTotalButacas(String.valueOf(codigo_sala));
					for (Asiento asiento : a) {
						this.getFdi().agregarFuncionAsiento(f2, asiento);

					}

					JOptionPane.showMessageDialog(this.getVistaAltaFuncion(), "Funci�n agregada correctamente",
							"Sistema", JOptionPane.INFORMATION_MESSAGE);
					this.getVistaAltaFuncion().dispose();

				}
			} else {
				JOptionPane.showMessageDialog(this.getVistaAltaFuncion(), "Complete todos los campos", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			}

		} else {
			this.restablecerCampos();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@SuppressWarnings("deprecation")
	public String descuento(Date d) {
		String[] strDAys = new String[] { "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" };
		String dia = strDAys[d.getDay()];
		return dia;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		JTextField jtf = (JTextField) e.getSource();
		char c = e.getKeyChar();
		if (jtf == this.getVistaAltaFuncion().getTxtFieldCodigoFuncion()) {
			if ((!Character.isDigit(c))
					|| (this.getVistaAltaFuncion().getTxtFieldCodigoFuncion().getText().length() > 3)) {
				this.getVistaAltaFuncion().getToolkit().beep();
				e.consume();
			}
		} else {
			if ((!Character.isDigit(c)) || (this.getVistaAltaFuncion().getTxtFieldPrecio().getText().length() > 3)) {
				this.getVistaAltaFuncion().getToolkit().beep();
				e.consume();
			}
		}

	}

	private Integer obtenerCodigo(String cad) {
		String cadenaCodigo = "";
		if (cad.contains("-")) {
			String[] parts = cad.split("-");
			cadenaCodigo = parts[0];
		}
		Integer i = Integer.parseInt(cadenaCodigo);
		return i;
	}

	public String setearHora(String hora) {

		Integer i = Integer.parseInt(hora);
		String min = "";
		if (i <= 9) {
			min = "0" + i;
			return min;
		} else {
			return hora;
		}
	}

	public void restablecerCampos() {
		this.getVistaAltaFuncion().getTxtFieldCodigoFuncion().setText("");
		this.getVistaAltaFuncion().getTextFieldNombreFuncion().setText("");
		this.getVistaAltaFuncion().getCmboBxPelicula().setToolTipText("");
		this.getVistaAltaFuncion().getCmboBxSala().setToolTipText("");
		this.getVistaAltaFuncion().getTxtFieldPrecio().setText("");

	}

	public void mostrarImagen(String dire) {
		File f = new File(dire);

		Image foto = null;
		try {
			foto = ImageIO.read(f).getScaledInstance(this.getVistaAltaFuncion().getLblImagen().getWidth(),
					this.getVistaAltaFuncion().getLblImagen().getHeight(), Image.SCALE_DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.getVistaAltaFuncion().getLblImagen().setIcon(new ImageIcon(foto));
		this.getVistaAltaFuncion().getLblImagen().updateUI();

	}

	@Override
	public void windowOpened(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@SuppressWarnings("unchecked")
	@Override
	public void windowActivated(WindowEvent e) {
		this.getFdi().consultarPeliculas(this.getVistaAltaFuncion().getCmboBxPelicula());
		this.getFdi().consultarSalas(this.getVistaAltaFuncion().getCmboBxSala());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String fecha = sdf.format(date);
		try {
			Date dato = null;
			dato = (Date) sdf.parse(fecha);
			this.getVistaAltaFuncion().getDateChooser().setDate(dato);
		} catch (ParseException ex) {
			ex.printStackTrace();
		}

		this.setearComponentes();

	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	public void setearComponentes() {
		String cade = this.getVistaAltaFuncion().getCmboBxPelicula().getSelectedItem().toString();
		Integer i = this.obtenerCodigo(cade);
		String ruta = System.getProperty("user.dir");
		String ruta2 = "\\src\\imagenesPeliculas\\";
		this.mostrarImagen(ruta + ruta2 + i + ".jpg");

		this.getVistaAltaFuncion().getTextAreaDescripcion().setText(this.getFdi().consultarDescripcion(i));
	}

	public String fechaMeses(String f) {

		String cadenaCodigo = "";
		String fecha = "";
		Integer i = 0;
		String[] parts = f.split("/");
		cadenaCodigo = parts[1];

		switch (cadenaCodigo) {
		case "ene":
			i = 1;
			break;
		case "feb":
			i = 2;
			break;
		case "mar":
			i = 3;
			break;
		case "abr":
			i = 4;
			break;
		case "may":
			i = 5;
			break;
		case "jun":
			i = 6;
			break;
		case "jul":
			i = 7;
			break;
		case "ago":
			i = 8;
			break;
		case "sep":
			i = 9;
			break;
		case "oct":
			i = 10;
			break;
		case "nov":
			i = 11;
			break;
		case "dic":
			i = 12;
			break;
		}

		fecha = parts[0] + "/" + i + "/" + parts[2];
		return fecha;

	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		this.setearComponentes();

	}

	public FuncionAlta getVistaAltaFuncion() {
		return vistaAltaFuncion;
	}

	public void setVistaAltaFuncion(FuncionAlta vistaAltaFuncion) {
		this.vistaAltaFuncion = vistaAltaFuncion;
	}

	public FuncionDaoImpl getFdi() {
		return fdi;
	}

	public void setFdi(FuncionDaoImpl fdi) {
		this.fdi = fdi;
	}

	public AsientoDaoImp getAdi() {
		return adi;
	}

	public void setAdi(AsientoDaoImp adi) {
		this.adi = adi;
	}

}
