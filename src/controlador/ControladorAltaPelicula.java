package controlador;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import modelo.Pelicula;
import modelo.PeliculaDaoImp;
import vista.PeliculaAlta;

public class ControladorAltaPelicula implements ActionListener, KeyListener {

	private PeliculaAlta pa;
	private PeliculaDaoImp pdi = new PeliculaDaoImp();

	public ControladorAltaPelicula() {

		this.setPa(new PeliculaAlta(this));
		this.getPa().setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		switch (btn.getText()) {
		case "Elegir":

			this.getPa().getLblImagen().setIcon(null);

			JFileChooser j = new JFileChooser();
			j.setFileSelectionMode(JFileChooser.FILES_ONLY);

			int estado = j.showOpenDialog(null);
			if (estado == JFileChooser.APPROVE_OPTION) {
				try {
					this.getPa().setFis(new FileInputStream(j.getSelectedFile()));

					try {
						Image icono = ImageIO.read(j.getSelectedFile()).getScaledInstance(
								this.getPa().getLblImagen().getWidth(), this.getPa().getLblImagen().getHeight(),
								Image.SCALE_DEFAULT);
						this.getPa().getLblImagen().setIcon(new ImageIcon(icono));
						this.getPa().getLblImagen().updateUI();

					} catch (IOException ex) {
						JOptionPane.showMessageDialog(this.getPa(), "imagen: " + ex);
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}

			break;
		case "Guardar":

			String nombre = this.getPa().getTxtNombre().getText();
			String audio = (String) this.getPa().getComboBoxAudio().getSelectedItem();
			String genero = (String) this.getPa().getComboBoxGenero().getSelectedItem();
			DecimalFormat df = new DecimalFormat("#.0");
			Double puntajeD = (Double) this.getPa().getSpinnerPuntaje().getValue();
			String puntaje = String.valueOf(df.format(puntajeD).replace(",", "."));
			String duracion = this.getPa().getTxtDuracion().getText();
			String condicion = (String) this.getPa().getComboBoxCondicion().getSelectedItem();
			String imagen = String.valueOf(this.getPdi().buscarUltimoCodigo()) + ".jpg";
			String estadoP = "Activo";
			String des = this.getPa().getAreaDescripcion().getText();

			Boolean validacion = (!nombre.equals("") && !puntaje.equals("") && !duracion.equals("")
					&& this.getPa().getLblImagen().getIcon() != null && !des.equals(""));
			if (validacion) {
				Pelicula p = new Pelicula(nombre, audio, genero, puntaje, duracion, condicion, imagen, estadoP, des);
				if (this.getPdi().agregarPelicula(p)) {
					JOptionPane.showMessageDialog(this.getPa(), "Pelicula agregada correctamente", "Sistema",
							JOptionPane.INFORMATION_MESSAGE);
					this.getPa().dispose();
					FileOutputStream fos = null;
					try {
						fos = new FileOutputStream(
								System.getProperty("user.dir") + ("\\src\\imagenesPeliculas\\") + imagen);
					} catch (FileNotFoundException e1) {

						e1.printStackTrace();
					}
					this.copiarImagen(this.getPa().getFis(), fos);

				}
			} else {
				JOptionPane.showMessageDialog(this.getPa(), "Complete los campos obligatorios (*)", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			}

			break;
		case "Cancelar":
			this.restablecerCampos();
		}

	}

	public void restablecerCampos() {
		this.getPa().getTxtCodigo().setText("");
		this.getPa().getTxtNombre().setText("");
		this.getPa().getComboBoxAudio().setSelectedIndex(0);
		this.getPa().getComboBoxGenero().setSelectedIndex(0);
		this.getPa().getSpinnerPuntaje().setValue(1.0);
		this.getPa().getTxtDuracion().setText("");
		this.getPa().getComboBoxCondicion().setSelectedIndex(0);
		this.getPa().getLblImagen().setIcon(null);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		JTextField jtf = (JTextField) e.getSource();
		char c = e.getKeyChar();
		if (jtf == this.getPa().getTxtCodigo()) {
			if ((!Character.isDigit(c)) || (this.getPa().getTxtCodigo().getText().length() > 3)) {

				e.consume();
			}

		} else {
			if ((!Character.isDigit(c)) || (this.getPa().getTxtDuracion().getText().length() > 2)) {

				e.consume();
			}

		}

	}

	public void copiarImagen(FileInputStream in, FileOutputStream out) {

		try {

			int c;

			try {
				while ((c = in.read()) != -1) {
					out.write(c);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public PeliculaDaoImp getPdi() {
		return pdi;
	}

	public void setPdi(PeliculaDaoImp pdi) {
		this.pdi = pdi;
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	public PeliculaAlta getPa() {
		return pa;
	}

	public void setPa(PeliculaAlta pa) {
		this.pa = pa;
	}
}
