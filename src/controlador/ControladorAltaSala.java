package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import modelo.Asiento;
import modelo.AsientoDaoImp;
import modelo.Sala;
import modelo.SalaDaoImp;
import vista.SalaAlta;

public class ControladorAltaSala implements ActionListener, KeyListener {

	private SalaAlta sa;
	private SalaDaoImp sdi = new SalaDaoImp();
	private Sala sala;
	private AsientoDaoImp adi = new AsientoDaoImp();

	public ControladorAltaSala() {
		this.setSa(new SalaAlta(this));
		this.getSa().setVisible(true);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		JTextField jtf = (JTextField) e.getSource();
		char c = e.getKeyChar();
		if (jtf == this.getSa().getTextFieldCodigo()) {
			if ((!Character.isDigit(c)) || (this.getSa().getTextFieldCodigo().getText().length() > 3)) {
				this.getSa().getToolkit().beep();
				e.consume();
			}
		} else if (jtf == this.getSa().getTextFieldCapHorizontal()) {
			if ((!Character.isDigit(c))) {
				this.getSa().getToolkit().beep();
				e.consume();
			}
		} else {
			if ((!Character.isDigit(c))) {
				this.getSa().getToolkit().beep();
				e.consume();
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getSa().getBtnGuardar()) {
			String codigo = this.getSa().getTextFieldCodigo().getText();
			String nombre = this.getSa().getTextFieldNombre().getText();
			String asientoV = this.getSa().getTextFieldCapVertical().getText();
			String asientoH = this.getSa().getTextFieldCapHorizontal().getText();

			Boolean validacion = (!codigo.equals("") && !nombre.equals("") && !asientoV.equals("")
					&& !asientoH.equals(""));
			if (validacion) {
				Sala s = new Sala(codigo, nombre);
				if (this.getSdi().agregarSala(s)) {
					JOptionPane.showMessageDialog(this.getSa(), "Sala agregada con exito", "Sistema",
							JOptionPane.INFORMATION_MESSAGE);
				}

			} else {
				JOptionPane.showMessageDialog(this.getSa(), "Complete todos los campos", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			}
			this.generarButacas(asientoV, asientoH);
		}

		else {
			this.restablecerCampos();
		}
	}

	public void restablecerCampos() {
		this.getSa().getTextFieldCodigo().setText("");
		this.getSa().getTextFieldNombre().setText("");
		this.getSa().getTextFieldCapHorizontal().setText("");
		this.getSa().getTextFieldCapVertical().setText("");
	}

	private void generarButacas(String asientoV, String asientoH) {
		Integer cantiButacas;
		String numButaca = "";
		cantiButacas = Integer.parseInt(asientoV) * Integer.parseInt(asientoH);
		for (int i = 1; i < cantiButacas + 1; i++) {
			numButaca = String.valueOf(i);
			Asiento a = new Asiento(numButaca, "true", this.getSa().getTextFieldCodigo().getText());
			this.getAdi().agregarAsiento(a);
		}
	}

	public SalaAlta getSa() {
		return sa;
	}

	public void setSa(SalaAlta sa) {
		this.sa = sa;
	}

	public SalaDaoImp getSdi() {
		return sdi;
	}

	public void setSdi(SalaDaoImp sdi) {
		this.sdi = sdi;
	}

	public Sala getSala() {
		return sala;
	}

	public void setSala(Sala sala) {
		this.sala = sala;
	}

	public AsientoDaoImp getAdi() {
		return adi;
	}

	public void setAdi(AsientoDaoImp adi) {
		this.adi = adi;
	}
	

}
