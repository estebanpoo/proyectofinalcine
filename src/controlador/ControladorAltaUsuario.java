package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import modelo.Usuario;
import modelo.UsuarioDaoImp;
import vista.UsuarioAlta;

public class ControladorAltaUsuario implements ActionListener, KeyListener {

	private UsuarioAlta ua;
	private UsuarioDaoImp udi = new UsuarioDaoImp();

	public ControladorAltaUsuario() {
		this.setUa(new UsuarioAlta(this));
		this.getUa().setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		if (btn.getText().equals("Guardar")) {

			String dni = this.getUa().getTxtDni().getText();
			String nombre = this.getUa().getTxtNombre().getText();
			String apellido = this.getUa().getTxtApellido().getText();
			String user = this.getUa().getTxtUser().getText();
			String tipo= this.getUa().getComboBoxTipo().getSelectedItem().toString();

			Boolean validacion = (!dni.equals("") && !nombre.equals("") && !apellido.equals("") && !user.equals(""));
			if (validacion) {
				Usuario u = new Usuario(dni, nombre, apellido, user,tipo);

				if (this.getUdi().agregarUsuario(u)) {
					JOptionPane.showMessageDialog(this.getUa(), "Usuario agregado correctamente", "Sistema",
							JOptionPane.INFORMATION_MESSAGE);
					this.getUa().dispose();
				}
			} else {
				JOptionPane.showMessageDialog(this.getUa(), "Complete los campos obligatorios (*)", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			}

		} else {
			this.restablecerCampos();

		}

	}

	public void restablecerCampos() {
		this.getUa().getTxtDni().setText("");
		this.getUa().getTxtNombre().setText("");
		this.getUa().getTxtApellido().setText("");
		this.getUa().getTxtUser().setText("");
	}

	@Override
	public void keyPressed(KeyEvent arg0) {

	}

	@Override
	public void keyReleased(KeyEvent arg0) {

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		char c = arg0.getKeyChar();
		if ((!Character.isDigit(c)) || (this.getUa().getTxtDni().getText().length() > 7)) {
			this.getUa().getToolkit().beep();
			arg0.consume();
		}

	}

	public UsuarioDaoImp getUdi() {
		return udi;
	}

	public void setUdi(UsuarioDaoImp udi) {
		this.udi = udi;
	}

	public UsuarioAlta getUa() {
		return ua;
	}

	public void setUa(UsuarioAlta ua) {
		this.ua = ua;
	}

}
