package controlador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import modelo.Funcion;
import modelo.FuncionDaoImpl;
import vista.VistaFuncion;

public class ControladorFuncion implements ActionListener, KeyListener, WindowListener {

	private VistaFuncion vFuncion;
	private FuncionDaoImpl fdi;

	public ControladorFuncion() {
		this.setFdi(new FuncionDaoImpl());
		this.setvFuncion(new VistaFuncion(this));
		this.getvFuncion().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource() == this.getvFuncion().getBtnAgregar()) {
			ControladorAltaFuncion caF = new ControladorAltaFuncion();
			caF.getVistaAltaFuncion().setVisible(true);

		}
		if (arg0.getSource() == this.getvFuncion().getBtnModificar()) {
			int filam = this.getvFuncion().getTable().getSelectedRow();
			if (filam == -1) {
				JOptionPane.showMessageDialog(this.getvFuncion(), "Debe seleccionar una fila", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			} else {
				ControladorModificacionFuncion cmF = new ControladorModificacionFuncion();
				Integer cod= (Integer) this.getvFuncion().getTable().getValueAt(filam, 0);
				cmF.getVistafM().getTxtFieldCodigoFuncion().setText(String.valueOf(cod) );
				cmF.getVistafM().getTextFieldNombreFuncion().setText((String) this.getvFuncion().getTable().getValueAt(filam, 2));
				SimpleDateFormat formatodeltexto = new SimpleDateFormat("dd/MM/yyyy");
				String fecha = this.getvFuncion().getTable().getValueAt(filam, 4).toString().trim();
				Date dato = null;
				try {
					dato = (Date) formatodeltexto.parse(fecha);
					cmF.getVistafM().getDateChooser().setDate(dato);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				String hora = obtenerHora((String) this.getvFuncion().getTable().getValueAt(filam, 5));
				String minutos = obtenerMinutos((String) this.getvFuncion().getTable().getValueAt(filam, 5));
				String sala= (String) this.getvFuncion().getTable().getValueAt(filam, 3);
				
				Integer peli= (Integer) this.getvFuncion().getTable().getValueAt(filam, 1);
				
				cmF.getVistafM().getCmboBxSala().getModel().setSelectedItem(this.getFdi().consultarSalas(sala));
				cmF.getVistafM().getCmboBxPelicula().getModel().setSelectedItem(this.getFdi().consultarPeliculas(peli));
				cmF.getVistafM().getSpinerHora().setValue(Integer.parseInt(hora));
				
				
				cmF.getVistafM().getSpinerMin().setValue(Integer.parseInt(minutos));
				cmF.getVistafM().getTxtFieldPrecio().setText((String) this.getvFuncion().getTable().getValueAt(filam, 6));
				cmF.setSala(sala);
				cmF.getVistafM().setVisible(true);
				
			}

		}

		if (arg0.getSource() == this.getvFuncion().getBtnEliminar()) {

			int filam = this.getvFuncion().getTable().getSelectedRow();
			if (filam == -1) {
				JOptionPane.showMessageDialog(this.getvFuncion(), "Debe seleccionar una fila", "Sistema",
						JOptionPane.ERROR_MESSAGE);

			} else {
				int i = JOptionPane.showConfirmDialog(this.getvFuncion(), "�Desea eliminar la funci�n?");
				if (i == JOptionPane.YES_OPTION) {
					Integer cod =  (Integer) this.getvFuncion().getTable().getValueAt(filam, 0);
					if (this.getFdi().borrarFuncion(cod)) {
						JOptionPane.showMessageDialog(this.getvFuncion(), "Funci�n eliminada correctamente", "Sistema",
								JOptionPane.INFORMATION_MESSAGE);
					}

				}
			}
		}
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		
		this.ventanaActivada();

	}

	public void ventanaActivada() {
		String[] columnas = { "C�digo Funci�n", "C�digo Pel�cula", "Nombre Funci�n", "C�digo Sala", "Fecha Funci�n",
				"Horario Funci�n", "Precio", "Descuento" };
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.setColumnIdentifiers(columnas);
		List<Funcion> funciones = this.getFdi().buscarFunciones();
		for (Funcion f : funciones) {
			Object[] row = new Object[8];
			row[0] = f.getCodigoF();
			row[1] = f.getPelicula();
			row[2] = f.getNombreFuncion();
			row[3] = f.getSala();
			row[4] = f.getFechaFuncion();
			row[5] = f.getHorarioFuncion();
			row[6] = f.getPrecio();
			row[7] = f.getDescuento();
			modelo.addRow(row);

		}
		this.getvFuncion().getTable().setModel(modelo);
	}

	private String obtenerHora(String cad) {
		String cadena = "";
		if (cad.contains(":")) {
			String[] parts = cad.split(":");
			cadena = parts[0];
		}
		return cadena;
	}

	private String obtenerMinutos(String cad) {
		String cadena = "";
		if (cad.contains(":")) {
			String[] parts = cad.split(":");
			cadena = parts[1];
		}
		return cadena;
	}


	@Override
	public void windowClosed(WindowEvent arg0) {

	}

	@Override
	public void windowClosing(WindowEvent arg0) {

	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {

	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {

	}

	@Override
	public void windowIconified(WindowEvent arg0) {

	}

	@Override
	public void windowOpened(WindowEvent arg0) {

	}

	@Override
	public void keyPressed(KeyEvent arg0) {

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		if (!this.getvFuncion().getTxtBuscar().getText().isEmpty()) {

			String cad = this.getvFuncion().getTxtBuscar().getText();
			String[] columnas = { "C�digo Funci�n", "C�digo Pelicula", "Nombre Funci�n", "C�digo Sala", "Fecha Funci�n",
					"Horario Funci�n", "Precio", "Descuento" };

			DefaultTableModel modelo = new DefaultTableModel();
			modelo.setColumnIdentifiers(columnas);
			List<Funcion> funciones = this.getFdi().buscarFuncion2(cad);
			for (Funcion f : funciones) {
				Object[] row = new Object[8];
				row[0] = f.getCodigoF();
				row[1] = f.getPelicula();
				row[2] = f.getNombreFuncion();
				row[3] = f.getSala();
				row[4] = f.getFechaFuncion();
				row[5] = f.getHorarioFuncion();
				row[6] = f.getPrecio();
				row[7] = f.getDescuento();
				modelo.addRow(row);

			}
			this.getvFuncion().getTable().setModel(modelo);
		} else {
			this.ventanaActivada();
		}

	}

	@Override
	public void keyTyped(KeyEvent arg0) {

	}

	public VistaFuncion getvFuncion() {
		return vFuncion;
	}

	public void setvFuncion(VistaFuncion vFuncion) {
		this.vFuncion = vFuncion;
	}

	public FuncionDaoImpl getFdi() {
		return fdi;
	}

	public void setFdi(FuncionDaoImpl fdi) {
		this.fdi = fdi;
	}
	

}