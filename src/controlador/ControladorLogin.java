package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import modelo.Usuario;
import modelo.UsuarioDaoImp;
import vista.VistaLogin;

public class ControladorLogin implements ActionListener {

	private VistaLogin vistaLogin;
	private Usuario usuario;
	private UsuarioDaoImp udi = new UsuarioDaoImp();
	private ControladorPrincipal cp;

	public ControladorLogin() {

		this.setVistaLogin(new VistaLogin(this));
		this.getVistaLogin().setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		if (btn.getText().equals("Salir")) {
			System.exit(0);
		} else {
			this.validar();

		}
	}

	@SuppressWarnings("deprecation")
	public void validar() {
		String dni = this.getVistaLogin().getTxtContrase�a().getText();
		String user = this.getVistaLogin().getTxtUsuario().getText();
		Usuario usuario = this.getUdi().validarUsuario(dni, user);

		if (dni.equals("") || user.equals("")) {

			JOptionPane.showMessageDialog(this.getVistaLogin(), "No dejar campos en blanco", "Sistema",
					JOptionPane.ERROR_MESSAGE);

		}

		else if (usuario.getDniUsuario() != null && usuario.getNombreUsuario() != null) {
			this.getVistaLogin().dispose();
			ControladorPrincipal cp = new ControladorPrincipal();
			cp.usuarioLogeado(usuario);
			cp.setUsuario(usuario.getDniUsuario());
			if(usuario.getTipo().equals("Administrador")) {
				
			
			cp.getVistaPrincipal().setVisible(true);
			}
			else {
				cp.getVistaPrincipal().getBtnFuncion().setEnabled(false);
				cp.getVistaPrincipal().getBtnPelicula().setEnabled(false);
				cp.getVistaPrincipal().getBtnReporte().setEnabled(false);
				cp.getVistaPrincipal().getBtnSala().setEnabled(false);
				cp.getVistaPrincipal().getBtnUsuario().setEnabled(false);
				cp.getVistaPrincipal().setVisible(true);
				
			}
			
		} else {
			JOptionPane.showMessageDialog(this.getVistaLogin(), "Datos incorrectos", "Sistema",
					JOptionPane.ERROR_MESSAGE);

			this.getVistaLogin().getTxtUsuario().requestFocus();
			this.getVistaLogin().getTxtUsuario().setText("");
			this.getVistaLogin().getTxtContrase�a().setText("");
		}
	}

	public VistaLogin getVistaLogin() {
		return vistaLogin;
	}

	public void setVistaLogin(VistaLogin vistaLogin) {
		this.vistaLogin = vistaLogin;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public UsuarioDaoImp getUdi() {
		return udi;
	}

	public void setUdi(UsuarioDaoImp udi) {
		this.udi = udi;
	}

	public ControladorPrincipal getCp() {
		return cp;
	}

	public void setCp(ControladorPrincipal cp) {
		this.cp = cp;
	}

}
