package controlador;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import modelo.Funcion;
import modelo.FuncionDaoImpl;
import vista.FuncionModificar;


public class ControladorModificacionFuncion implements ActionListener, KeyListener, WindowListener, ItemListener {

	private FuncionModificar vistafM;
	private FuncionDaoImpl fdi = new FuncionDaoImpl();
	private String sala;
	private Integer pelicula;

	public ControladorModificacionFuncion() {
		this.setVistafM(new FuncionModificar(this));
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getVistafM().getBtnGuardar()) {
			Integer codigo = Integer.parseInt(this.getVistafM().getTxtFieldCodigoFuncion().getText());
			String nombre_funcion = this.getVistafM().getTextFieldNombreFuncion().getText();
			String pelicula = (String) this.getVistafM().getCmboBxPelicula().getSelectedItem();
			String sala = (String) this.getVistafM().getCmboBxSala().getSelectedItem();

			String formato = this.getVistafM().getDateChooser().getDateFormatString();
			Date fdate = this.getVistafM().getDateChooser().getDate();
			SimpleDateFormat sdf = new SimpleDateFormat(formato);

			String fechaFuncion = String.valueOf(sdf.format(fdate));
			String horario = this.setearHora(this.getVistafM().getSpinerHora().getValue().toString()) + ":"+ this.setearHora(this.getVistafM().getSpinerMin().getValue().toString());
			String precio = this.getVistafM().getTxtFieldPrecio().getText();
			Integer codigo_pelicula = this.obtenerCodigo(pelicula);
			String codigo_sala = String.valueOf(this.obtenerCodigo(sala));

			Boolean validacion = (!codigo_pelicula.equals("") && !codigo_sala.equals("")
					&& !horario.equals("") && !precio.equals("") && !nombre_funcion.equals("")
					&& !fechaFuncion.equals(""));
			if (validacion) {

				String descuento;
				if (this.descuento(fdate).equals("Jueves") || this.descuento(fdate).equals("Martes")) {
					Double d = Double.parseDouble(precio);
					d = d * 0.25;
					descuento = String.valueOf(d);
				} else {
					descuento = "0";
				}

				Funcion f = new Funcion(codigo, codigo_pelicula, nombre_funcion, codigo_sala, fechaFuncion, horario,
						precio, descuento);
				if (this.getFdi().modificarFuncion(f)) {
					JOptionPane.showMessageDialog(this.getVistafM(), "Funcion modificada correctamente", "Sistema",
							JOptionPane.INFORMATION_MESSAGE);
					this.getVistafM().dispose();

				}
			} else {
				JOptionPane.showMessageDialog(this.getVistafM(), "Complete todos los campos", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			}

		} else {

			restablecerCampos();

		}
	}

	public void restablecerCampos() {
		this.getVistafM().getTxtFieldCodigoFuncion().setText("");
		this.getVistafM().getTextFieldNombreFuncion().setText("");
		this.getVistafM().getCmboBxPelicula().setToolTipText("");
		this.getVistafM().getCmboBxSala().setToolTipText("");
		this.getVistafM().getTxtFieldPrecio().setText("");

	}

	private Integer obtenerCodigo(String cad) {
		String cadenaCodigo = "";
		
			String[] parts = cad.split("-");
			cadenaCodigo = parts[0];
		Integer i= Integer.parseInt(cadenaCodigo);
	
		return i;
	}

	public String setearHora(String hora) {

		Integer i = Integer.parseInt(hora);
		String min = "";
		if (i <= 9) {
			min = "0" + i;
			return min;
		} else {
			return hora;
		}
	}

	@SuppressWarnings("deprecation")
	public String descuento(Date d) {
		String[] strDAys = new String[] { "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" };
		String dia = strDAys[d.getDay()];
		return dia;

	}
	
	public void mostrarImagen(String dire) {
		File f = new File(dire);

		Image foto = null;
		try {
			foto = ImageIO.read(f).getScaledInstance(this.getVistafM().getLblImagen().getWidth(), this.getVistafM().getLblImagen().getHeight(),
					Image.SCALE_DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.getVistafM().getLblImagen().setIcon(new ImageIcon(foto));
		this.getVistafM().getLblImagen().updateUI();

	}
	
	public void setearComponentes () {
		String cade= this.getVistafM().getCmboBxPelicula().getSelectedItem().toString();
		Integer i = this.obtenerCodigo(cade);
		String ruta = System.getProperty("user.dir");
		String ruta2 = "\\src\\imagenesPeliculas\\";
		this.mostrarImagen(ruta+ruta2+i+".jpg");
		
		this.getVistafM().getTextAreaDescripcion().setText(this.getFdi().consultarDescripcion(i));
	}


	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void windowOpened(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@SuppressWarnings("unchecked")
	@Override
	public void windowActivated(WindowEvent e) {

		this.getFdi().consultarPeliculas(this.getVistafM().getCmboBxPelicula());
		this.getFdi().consultarSalas(this.getVistafM().getCmboBxSala());
		this.getVistafM().getCmboBxSala().setSelectedItem(this.getSala());
		this.setearComponentes();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		this.setearComponentes();
		
	}
	

	public FuncionModificar getVistafM() {
		return vistafM;
	}

	public void setVistafM(FuncionModificar vistafM) {
		this.vistafM = vistafM;
	}

	public FuncionDaoImpl getFdi() {
		return fdi;
	}

	public void setFdi(FuncionDaoImpl fdi) {
		this.fdi = fdi;
	}

	public String getSala() {
		return sala;
	}

	public void setSala(String sala) {
		this.sala = sala;
	}

	public Integer getPelicula() {
		return pelicula;
	}

	public void setPelicula(Integer pelicula) {
		this.pelicula = pelicula;
	}


}
