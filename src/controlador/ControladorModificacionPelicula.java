package controlador;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import modelo.Pelicula;
import modelo.PeliculaDaoImp;
import vista.PeliculaModificacion;

public class ControladorModificacionPelicula implements ActionListener, KeyListener, WindowListener {

	private PeliculaModificacion pm;
	private PeliculaDaoImp pdi = new PeliculaDaoImp();

	public ControladorModificacionPelicula() {
		this.setPm(new PeliculaModificacion(this));

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		JButton btn = (JButton) e.getSource();
		switch (btn.getText()) {
		case "Elegir":

			this.getPm().getLblImagen().setIcon(null);

			JFileChooser j = new JFileChooser();
			j.setFileSelectionMode(JFileChooser.FILES_ONLY);

			int estado = j.showOpenDialog(null);
			if (estado == JFileChooser.APPROVE_OPTION) {
				try {
					this.getPm().setFis(new FileInputStream(j.getSelectedFile()));

					try {
						Image icono = ImageIO.read(j.getSelectedFile()).getScaledInstance(
								this.getPm().getLblImagen().getWidth(), this.getPm().getLblImagen().getHeight(),
								Image.SCALE_DEFAULT);
						this.getPm().getLblImagen().setIcon(new ImageIcon(icono));
						this.getPm().getLblImagen().updateUI();

					} catch (IOException ex) {
						JOptionPane.showMessageDialog(this.getPm(), "imagen: " + ex);
					}
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}

			break;

		case "Guardar":

			Integer codigo = Integer.parseInt(this.getPm().getTxtCodigo().getText());
			String nombre = this.getPm().getTxtNombre().getText();
			String audio = (String) this.getPm().getComboBoxAudio().getSelectedItem();
			String genero = (String) this.getPm().getComboBoxGenero().getSelectedItem();

			DecimalFormat df = new DecimalFormat("#.0");

			Double puntajeD = (Double) this.getPm().getSpinnerPuntaje().getValue();
			String puntaje = String.valueOf(df.format(puntajeD).replace(",", "."));
			String duracion = this.getPm().getTxtDuracion().getText();
			String condicion = (String) this.getPm().getComboBoxCondicion().getSelectedItem();
			String imagen = codigo + ".jpg";
			String des= this.getPm().getAreaDescripcion().getText();
			

			Boolean validacion = (!nombre.equals("") && !puntaje.equals("") && !duracion.equals("")
					&& this.getPm().getLblImagen().getIcon() != null);
			if (validacion) {

				Pelicula p = new Pelicula(codigo, nombre, audio, genero, puntaje, duracion, condicion, imagen,des);
				if (this.getPdi().modificarPelicula(p)) {
					FileOutputStream fos;

					try {

						if (this.getPm().getFis() != null) {

							fos = new FileOutputStream(
									System.getProperty("user.dir") + ("\\src\\imagenesPeliculas\\") + codigo + ".jpg");
							String pelicula = codigo + ".jpg";

							this.copiarImagen(this.getPm().getFis(), fos);
							try {
								fos.close();
							} catch (IOException e1) {

								e1.printStackTrace();
							}
						}

						JOptionPane.showMessageDialog(this.getPm(), "Pelicula modificada correctamente", "Sistema",
								JOptionPane.INFORMATION_MESSAGE);
						this.getPm().dispose(); // si no anda cambiar esto

					} catch (FileNotFoundException e1) {

						e1.printStackTrace();
					}

				}
			} else {
				JOptionPane.showMessageDialog(this.getPm(), "Complete los campos obligatorios (*)", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			}

			break;

		case "Cancelar":
			this.restablecerCampos();
			break;

		}

	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	public void restablecerCampos() {
		this.getPm().getTxtCodigo().setText("");
		this.getPm().getTxtNombre().setText("");
		this.getPm().getComboBoxAudio().setSelectedIndex(0);
		this.getPm().getComboBoxGenero().setSelectedIndex(0);
		this.getPm().getSpinnerPuntaje().setValue(1.0);
		this.getPm().getTxtDuracion().setText("");
		this.getPm().getComboBoxCondicion().setSelectedIndex(0);
		this.getPm().getLblImagen().setIcon(null);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		JTextField jtf = (JTextField) e.getSource();
		char c = e.getKeyChar();
		if (jtf == this.getPm().getTxtCodigo()) {
			if ((!Character.isDigit(c)) || (this.getPm().getTxtCodigo().getText().length() > 3)) {

				e.consume();
			}

		} else {
			if ((!Character.isDigit(c)) || (this.getPm().getTxtDuracion().getText().length() > 2)) {

				e.consume();
			}

		}

	}

	public void copiarImagen(FileInputStream in, FileOutputStream out) {

		try {

			int c;

			try {
				while ((c = in.read()) != -1) {
					out.write(c);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void windowActivated(WindowEvent arg0) {

	}

	@Override
	public void windowClosed(WindowEvent arg0) {

	}

	@Override
	public void windowClosing(WindowEvent arg0) {

	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {

	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {

	}

	@Override
	public void windowIconified(WindowEvent arg0) {

	}

	@Override
	public void windowOpened(WindowEvent arg0) {

	}
	public PeliculaModificacion getPm() {
		return pm;
	}

	public void setPm(PeliculaModificacion pm) {
		this.pm = pm;
	}

	public PeliculaDaoImp getPdi() {
		return pdi;
	}

	public void setPdi(PeliculaDaoImp pdi) {
		this.pdi = pdi;
	}

}
