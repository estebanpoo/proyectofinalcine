package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;

import modelo.Sala;
import modelo.SalaDaoImp;
import vista.SalaModificar;

public class ControladorModificacionSala implements ActionListener, KeyListener {

	private SalaModificar sm;
	private SalaDaoImp sdi = new SalaDaoImp();

	public ControladorModificacionSala() {
		this.setSm(new SalaModificar(this));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getSm().getBtnGuardar()) {
			String codigo = this.getSm().getTextFieldCodigo().getText();
			String nombre = this.getSm().getTextFieldNombre().getText();
			String asientoV = this.getSm().getTextFieldCapVertical().getText();
			String asientoH = this.getSm().getTextFieldCapHorizontal().getText();
			Boolean validacion = (!codigo.equals("") && !nombre.equals("") && !asientoV.equals("")
					&& !asientoH.equals(""));
			if (validacion) {
				Sala s = new Sala(codigo, nombre);
				if (this.getSdi().modificarSala(s)) {
					JOptionPane.showMessageDialog(this.getSm(), "Sala modificada correctamente", "Sistema",
							JOptionPane.INFORMATION_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(this.getSm(), "Complete todos los campos", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			}
		} else {
			this.restablecerCampos();
		}
	}

	public void restablecerCampos() {
		this.getSm().getTextFieldCodigo().setText("");
		this.getSm().getTextFieldNombre().setText("");
		this.getSm().getTextFieldCapVertical().setText("");
		this.getSm().getTextFieldCapHorizontal().setText("");
	}
	

	@Override
	public void keyTyped(KeyEvent e) {
		char c = e.getKeyChar();
		if ((!Character.isDigit(c)) || (this.getSm().getTextFieldCodigo().getText().length() > 3)) {
			this.getSm().getToolkit().beep();
			e.consume();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	public SalaModificar getSm() {
		return sm;
	}

	public void setSm(SalaModificar sm) {
		this.sm = sm;
	}

	public SalaDaoImp getSdi() {
		return sdi;
	}

	public void setSdi(SalaDaoImp sdi) {
		this.sdi = sdi;
	}
}
