package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import modelo.Usuario;
import modelo.UsuarioDaoImp;
import vista.UsuarioModificacion;

public class ControladorModificacionUsuario implements ActionListener, KeyListener {

	private UsuarioModificacion um;
	private UsuarioDaoImp udi = new UsuarioDaoImp();

	public ControladorModificacionUsuario() {

		this.setUm(new UsuarioModificacion(this));

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();

		if (btn.getText().equals("Guardar")) {
			Integer id= Integer.parseInt(this.getUm().getTxtID().getText());
			String dni = this.getUm().getTxtDni().getText();
			String nombre = this.getUm().getTxtNombre().getText();
			String apellido = this.getUm().getTxtApellido().getText();
			String user = this.getUm().getTxtUser().getText();
			String tipo= this.getUm().getComboBoxTipo().getSelectedItem().toString();

			Boolean validacion = (!nombre.equals("") && !apellido.equals("") && !user.equals(""));
			if (validacion) {
				Usuario u = new Usuario(id,dni, nombre, apellido, user,tipo);

				if (this.getUdi().modificarUsuario(u)) {
					JOptionPane.showMessageDialog(this.getUm(), "Usuario modificado correctamente", "Sistema",
							JOptionPane.INFORMATION_MESSAGE);
					this.getUm().dispose();
				}
			} else {
				JOptionPane.showMessageDialog(this.getUm(), "Complete los campos obligatorios (*)", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			}

		} else {

			this.restablecerCampos();

		}

	}

	public void restablecerCampos() {
		this.getUm().getTxtDni().setText("");
		this.getUm().getTxtNombre().setText("");
		this.getUm().getTxtApellido().setText("");
		this.getUm().getTxtUser().setText("");
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	public UsuarioModificacion getUm() {
		return um;
	}

	public void setUm(UsuarioModificacion um) {
		this.um = um;
	}

	public UsuarioDaoImp getUdi() {
		return udi;
	}

	public void setUdi(UsuarioDaoImp udi) {
		this.udi = udi;
	}

}
