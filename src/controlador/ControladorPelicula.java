package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import modelo.Pelicula;
import modelo.PeliculaDaoImp;
import vista.VistaPelicula;

public class ControladorPelicula implements ActionListener, WindowListener, KeyListener {

	private VistaPelicula vPelicula;
	private PeliculaDaoImp pdi = new PeliculaDaoImp();


	public ControladorPelicula() {
		this.setvPelicula(new VistaPelicula(this));
		this.getvPelicula().setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		switch (btn.getText()) {
		case "Agregar":
			new ControladorAltaPelicula();

			break;
		case "Modificar":
			int fila = this.getvPelicula().getTable().getSelectedRow();
			if (fila == -1) {
				JOptionPane.showMessageDialog(this.getvPelicula(), "Debe seleccionar una pelicula", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			} else {
				ControladorModificacionPelicula cmp = new ControladorModificacionPelicula();
				Integer cod=(Integer) this.getvPelicula().getTable().getValueAt(fila, 0);
				cmp.getPm().getTxtCodigo().setText((cod.toString()) );
				cmp.getPm().getTxtNombre().setText((String) this.getvPelicula().getTable().getValueAt(fila, 1));
				cmp.getPm().getComboBoxAudio().setSelectedItem(this.getvPelicula().getTable().getValueAt(fila, 2));
				cmp.getPm().getComboBoxGenero().setSelectedItem(this.getvPelicula().getTable().getValueAt(fila, 3));
				String puntajeS = (String) this.getvPelicula().getTable().getValueAt(fila, 4);
				Double puntajeD = Double.parseDouble(puntajeS);
				cmp.getPm().getSpinnerPuntaje().setValue(puntajeD);
				cmp.getPm().getTxtDuracion().setText((String) this.getvPelicula().getTable().getValueAt(fila, 5));
				cmp.getPm().getComboBoxCondicion().setSelectedItem(this.getvPelicula().getTable().getValueAt(fila, 6));
				cmp.getPm().mostrarImagen(System.getProperty("user.dir") + ("\\src\\imagenesPeliculas\\")
						+ (String) this.getvPelicula().getTable().getValueAt(fila, 7));
				cmp.getPm().setFis(null);
				cmp.getPm().getAreaDescripcion().setText((String) this.getvPelicula().getTable().getValueAt(fila, 9));
				cmp.getPm().setVisible(true);
				

			}

			break;
		case "Eliminar":
			int filap = this.getvPelicula().getTable().getSelectedRow();
			if (filap == -1) {
				JOptionPane.showMessageDialog(this.getvPelicula(), "Debe seleccionar una pelicula", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			} else {
				int i = JOptionPane.showConfirmDialog(this.getvPelicula(), "�Desea eliminar la pelicula?");
				if (i == JOptionPane.YES_OPTION) {

					Integer codigo =  (Integer) this.getvPelicula().getTable().getValueAt(filap, 0);
					if (this.getPdi().borrarPelicula(codigo)) {
						JOptionPane.showMessageDialog(this.getvPelicula(), "Pelicula eliminada correctamente",
								"Sistema", JOptionPane.INFORMATION_MESSAGE);

					}
				}
			}
			break;

		}

	}

	@Override
	public void windowActivated(WindowEvent e) {
		this.ventanaActivada();
	}

	public void ventanaActivada() {
		String[] columnas = {"C�digo", "Nombre", "Audio", "G�nero", "Puntaje", "Duraci�n (min)", "Condici�n", "Imagen",
				"Fecha de alta" ,"Descripci�n"};
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.setColumnIdentifiers(columnas);
		List<Pelicula> peliculas = this.getPdi().buscarPeliculas();
		for (Pelicula u : peliculas) {
			Object[] row = new Object[10];
			
			row[0] = u.getId_pelicula();
			row[1] = u.getNombrePelicula();
			row[2] = u.getAudioDoble();
			row[3] = u.getGeneroPelicula();
			row[4] = u.getPuntajeCritica();
			row[5] = u.getDuracionPelicula();
			row[6] = u.getCondicionPelicula();
			row[7] = u.getImagenPelicula();
			row[8] = u.getFecha();
			row[9] = u.getDescripcion();
			modelo.addRow(row);

		}

		this.getvPelicula().getTable().setModel(modelo);

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowOpened(WindowEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// ACA

		if (!this.getvPelicula().getTxtBuscar().getText().isEmpty()) {

			String cad = this.getvPelicula().getTxtBuscar().getText();
			String[] columnas = { "C�digo", "Nombre", "Audio", "Genero", "Puntaje", "Duraci�n (min)", "Condici�n",
					"Imagen", "Fecha de alta","Descripci�n" };
			DefaultTableModel modelo = new DefaultTableModel();
			modelo.setColumnIdentifiers(columnas);
			List<Pelicula> peliculas = this.getPdi().buscarPeliculas2(cad);
			for (Pelicula u : peliculas) {
				Object[] row = new Object[10];
				row[0] = u.getId_pelicula();
				row[1] = u.getNombrePelicula();
				row[2] = u.getAudioDoble();
				row[3] = u.getGeneroPelicula();
				row[4] = u.getPuntajeCritica();
				row[5] = u.getDuracionPelicula();
				row[6] = u.getCondicionPelicula();
				row[7] = u.getImagenPelicula();
				row[8] = u.getFecha();
				row[9] = u.getDescripcion();
				modelo.addRow(row);

			}

			this.getvPelicula().getTable().setModel(modelo);

		} else {
			this.ventanaActivada();

		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	public VistaPelicula getvPelicula() {
		return vPelicula;
	}

	public void setvPelicula(VistaPelicula vPelicula) {
		this.vPelicula = vPelicula;
	}

	public PeliculaDaoImp getPdi() {
		return pdi;
	}

	public void setPdi(PeliculaDaoImp pdi) {
		this.pdi = pdi;
	}


}