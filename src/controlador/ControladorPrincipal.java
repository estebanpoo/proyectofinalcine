package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import modelo.Usuario;
import vista.VistaPrincipal;

public class ControladorPrincipal implements ActionListener, WindowListener {

	private VistaPrincipal vistaPrincipal;
	private Usuario u=new Usuario();
	private String usuario;

	public ControladorPrincipal() {
		this.setVistaPrincipal(new VistaPrincipal(this));
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JButton btn = (JButton) arg0.getSource();
		switch (btn.getText()) {
		case "Ventas":
			ControladorVenta cv= new ControladorVenta();
			cv.setUsuario(this.getUsuario());
			cv.getvVenta().setVisible(true);
			break;
		case "Funciones":
			new ControladorFuncion();
			break;
		case "Pel�culas":
			new ControladorPelicula();
			break;
		case "Usuarios":
			new ControladorUsuario();
			break;
		case "Salas":
			new ControladorSala();
			break;
			
		case "Reportes":
			new ControladorReporte();
			break;

		}

	}

	public void cerrar() {

		int i = JOptionPane.showConfirmDialog(this.getVistaPrincipal(), "             �Desea salir?");
		if (JOptionPane.YES_OPTION == i) {

			System.exit(0);
		}

	}

	public void usuarioLogeado(Usuario u) {

		this.getVistaPrincipal().getLblLogeado().setText(this.getVistaPrincipal().getLblLogeado().getText()
				+ u.getApellidoUsuario() + " " + u.getNombreUsuario());

	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {
		this.cerrar();

	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowOpened(WindowEvent e) {

	}

	public VistaPrincipal getVistaPrincipal() {
		return vistaPrincipal;
	}

	public void setVistaPrincipal(VistaPrincipal vistaPrincipal) {
		this.vistaPrincipal = vistaPrincipal;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	

}
