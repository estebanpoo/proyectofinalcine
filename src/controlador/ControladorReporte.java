package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import vista.VistaReporte;


public class ControladorReporte implements ActionListener{
	
	private VistaReporte vistaReporte;
	
	public ControladorReporte() {
		this.setVistaReporte(new VistaReporte(this));
		this.getVistaReporte().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==this.getVistaReporte().getBtnReporteVentas()) {
		
		new ControladorReporteVenta();
		
		}
		if (e.getSource()==this.getVistaReporte().getBtnVentasVendedor()) {
			
			new ControladorReporteVentasVendedor();

		}
		}
			

	public VistaReporte getVistaReporte() {
		return vistaReporte;
	}




	public void setVistaReporte(VistaReporte vistaReporte) {
		this.vistaReporte = vistaReporte;
	}

}
