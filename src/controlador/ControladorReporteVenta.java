package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import basededatos.BD;
import modelo.FuncionDaoImpl;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import vista.VistaReporteVentas;

public class ControladorReporteVenta implements ActionListener, WindowListener {

	private VistaReporteVentas vrp;
	private FuncionDaoImpl fdi = new FuncionDaoImpl();

	public ControladorReporteVenta() {
		this.setVrp(new VistaReporteVentas(this));
		this.getVrp().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.getVrp().dispose();
		try {
			this.reporteVenta();
		} catch (JRException e1) {
			e1.printStackTrace();
		}

	}

	public void reporteVenta() throws JRException {
		String ruta = System.getProperty("user.dir");
		Map parametro = new HashMap<>();

		parametro.put("Parameter1", obtenerCodigo());
		parametro.put("Parameter2", this.obtenerFechaDesde());
		parametro.put("Parameter3", this.obtenerFechaHasta());

		JasperPrint jasperPrintWindow = JasperFillManager.fillReport(ruta + "\\reportes\\ReporteVenta.jasper",
				parametro, BD.getInstance().getConnection());
		JasperViewer jasperViewer = new JasperViewer(jasperPrintWindow, false);
		jasperViewer.setVisible(true);
	}

	private String obtenerFechaDesde() {
		String formato = this.getVrp().getDcDesde().getDateFormatString();
		Date fdate = this.getVrp().getDcDesde().getDate();
		SimpleDateFormat sdf = new SimpleDateFormat(formato);

		String fechaFuncion = String.valueOf(sdf.format(fdate));
		return fechaFuncion;
	}

	private String obtenerFechaHasta() {
		String formato = this.getVrp().getDcHasta().getDateFormatString();
		Date fdate = this.getVrp().getDcHasta().getDate();
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		String fechaFuncion = String.valueOf(sdf.format(fdate));
		return fechaFuncion;
	}

	private Integer obtenerCodigo() {
		String cad = this.getVrp().getComboBox().getSelectedItem().toString();
		String cadenaCodigo = "";
		String[] parts = cad.split("-");
		cadenaCodigo = parts[0];
		Integer i = Integer.parseInt(cadenaCodigo);
		return i;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void windowActivated(WindowEvent arg0) {
		this.getFdi().consultarPeliculas(this.getVrp().getComboBox());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String fecha = sdf.format(date);
		try {
			Date dato = null;
			dato = (Date) sdf.parse(fecha);
			this.getVrp().getDcDesde().setDate(dato);
			this.getVrp().getDcHasta().setDate(dato);

		} catch (ParseException ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public void windowClosed(WindowEvent arg0) {

	}

	@Override
	public void windowClosing(WindowEvent arg0) {

	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {

	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {

	}

	@Override
	public void windowIconified(WindowEvent arg0) {

	}

	@Override
	public void windowOpened(WindowEvent arg0) {

	}

	public VistaReporteVentas getVrp() {
		return vrp;
	}

	public void setVrp(VistaReporteVentas vrp) {
		this.vrp = vrp;
	}

	public FuncionDaoImpl getFdi() {
		return fdi;
	}

	public void setFdi(FuncionDaoImpl fdi) {
		this.fdi = fdi;
	}

}
