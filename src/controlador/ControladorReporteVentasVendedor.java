package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import basededatos.BD;
import modelo.UsuarioDaoImp;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import vista.VistaReporteVendedor;

public class ControladorReporteVentasVendedor implements ActionListener, WindowListener {
	
	private VistaReporteVendedor vrv;
	private UsuarioDaoImp udi=new UsuarioDaoImp();
	
	public ControladorReporteVentasVendedor() {
		this.setVrv(new VistaReporteVendedor(this));
		this.getVrv().setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		this.getVrv().dispose();
		try {
			
			this.reporteVendedor();
		} catch (JRException e1) {
			e1.printStackTrace();
}
		
	}


	@Override
	public void windowOpened(WindowEvent e) {
		
	}



	@Override
	public void windowClosing(WindowEvent e) {
		
	}


	@Override
	public void windowClosed(WindowEvent e) {
		
	}


	@Override
	public void windowIconified(WindowEvent e) {
		
	}


	@Override
	public void windowDeiconified(WindowEvent e) {
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public void windowActivated(WindowEvent e) {
		this.getUdi().consultarUsuario(this.getVrv().getComboBox());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String fecha=sdf.format(date);
		try {
			Date dato=null;
			dato = (Date) sdf.parse(fecha);
			this.getVrv().getDateChooserDesde().setDate(dato);
			this.getVrv().getDateChooserHasta().setDate(dato);
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
	}


	@Override
	public void windowDeactivated(WindowEvent e) {
		
	}


	
	public void reporteVendedor() throws JRException {
		String ruta = System.getProperty("user.dir");
		Map parametro = new HashMap<>();
		
		parametro.put("Parameter1", obtenerCodigo());
		parametro.put("Parameter2", this.obtenerFechaDesde());
		parametro.put("Parameter3", this.obtenerFechaHasta());
		
		JasperPrint jasperPrintWindow = JasperFillManager.fillReport(
		ruta+"\\reportes\\ReporteVendedor.jasper", parametro,
		BD.getInstance().getConnection());
		JasperViewer jasperViewer = new JasperViewer(jasperPrintWindow,false);
		jasperViewer.setVisible(true);
	}
	
	private String obtenerFechaDesde() {
		String formato = this.getVrv().getDateChooserDesde().getDateFormatString();
		Date fdate = this.getVrv().getDateChooserDesde().getDate();
		SimpleDateFormat sdf = new SimpleDateFormat(formato);

		String fechaVentas = String.valueOf(sdf.format(fdate));
		return fechaVentas;
	}
	private String obtenerFechaHasta() {
		String formato = this.getVrv().getDateChooserHasta().getDateFormatString();
		Date fdate = this.getVrv().getDateChooserHasta().getDate();
		SimpleDateFormat sdf = new SimpleDateFormat(formato);

		String fechaVentas = String.valueOf(sdf.format(fdate));
		return fechaVentas;
	}

	private Integer obtenerCodigo() {
		String cad = this.getVrv().getComboBox().getSelectedItem().toString();
		String cadenaCodigo="";
			String[] parts = cad.split("-");
			cadenaCodigo = parts[0];
		Integer i = Integer.parseInt(cadenaCodigo);
		return i;
	}


	public VistaReporteVendedor getVrv() {
		return vrv;
	}


	public void setVrv(VistaReporteVendedor vrv) {
		this.vrv = vrv;
	}


	public UsuarioDaoImp getUdi() {
		return udi;
	}


	public void setUdi(UsuarioDaoImp udi) {
		this.udi = udi;
	}
	
}
