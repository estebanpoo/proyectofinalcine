package controlador;

import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import modelo.AsientoDaoImp;
import modelo.Sala;
import modelo.SalaDaoImp;
import vista.VistaSala;

public class ControladorSala implements ActionListener, WindowListener {
	private VistaSala vSala;
	private SalaDaoImp spi;
	private AsientoDaoImp adi = new AsientoDaoImp();

	public ControladorSala() {
		this.setSpi(new SalaDaoImp());
		this.setvSala(new VistaSala(this));
		this.getvSala().setVisible(true);
	}

	@Override
	public void windowOpened(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowActivated(WindowEvent e) {
		this.ventanaActivada();
	}

	public void ventanaActivada() {
		String[] columnas = { "C�digo", "Nombre Sala" };
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.setColumnIdentifiers(columnas);
		List<Sala> salas = this.spi.buscarSala();
		for (Sala sala : salas) {
			Object[] row = new Object[2];
			row[0] = sala.getCodigoSala();
			row[1] = sala.getNombreSala();
			modelo.addRow(row);
		}
		this.getvSala().getTable().setModel(modelo);
	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getvSala().getBtnAgregar()) {
			new ControladorAltaSala();
		}
		if (e.getSource() == this.getvSala().getBtnModificar()) {
			int fila = this.getvSala().getTable().getSelectedRow();
			if (fila == -1) {
				JOptionPane.showMessageDialog(this.getvSala(), "Debe seleccionar una Sala", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			} else {
				ControladorModificacionSala cms = new ControladorModificacionSala();
				cms.getSm().getTextFieldCodigo().setText((String) this.getvSala().getTable().getValueAt(fila, 0));
				cms.getSm().getTextFieldNombre().setText((String) this.getvSala().getTable().getValueAt(fila, 1));
			}
			if (e.getSource() == this.getvSala().getBtnEliminar()) {

			}
		}
	}

	public VistaSala getvSala() {
		return vSala;
	}

	public void setvSala(VistaSala vSala) {
		this.vSala = vSala;
	}

	public SalaDaoImp getSpi() {
		return spi;
	}

	public void setSpi(SalaDaoImp spi) {
		this.spi = spi;
	}

	public AsientoDaoImp getAdi() {
		return adi;
	}

	public void setAdi(AsientoDaoImp adi) {
		this.adi = adi;
	}
	

}
