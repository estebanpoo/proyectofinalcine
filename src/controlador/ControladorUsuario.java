package controlador;

import java.awt.Checkbox;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import modelo.Usuario;
import modelo.UsuarioDaoImp;
import vista.VistaUsuario;

public class ControladorUsuario implements ActionListener, WindowListener, KeyListener {

	private VistaUsuario vUsuario;
	private UsuarioDaoImp udi;

	public ControladorUsuario() {
		this.setUdi(new UsuarioDaoImp());
		this.setvUsuario(new VistaUsuario(this));
		this.getvUsuario().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		JButton btn = (JButton) e.getSource();
		switch (btn.getText()) {
		case "Agregar":

			new ControladorAltaUsuario();

			break;

		case "Modificar":

			int filam = this.getvUsuario().getTable().getSelectedRow();
			if (filam == -1) {
				JOptionPane.showMessageDialog(this.getvUsuario(), "Debe seleccionar un usuario", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			} else {
				ControladorModificacionUsuario cmu = new ControladorModificacionUsuario();
				Integer id = (Integer) this.getvUsuario().getTable().getValueAt(filam, 0);
				cmu.getUm().getTxtID().setText(String.valueOf(id));
				cmu.getUm().getTxtDni().setText((String) this.getvUsuario().getTable().getValueAt(filam, 1));
				cmu.getUm().getTxtNombre().setText((String) this.getvUsuario().getTable().getValueAt(filam, 2));
				cmu.getUm().getTxtApellido().setText((String) this.getvUsuario().getTable().getValueAt(filam, 3));
				cmu.getUm().getTxtUser().setText((String) this.getvUsuario().getTable().getValueAt(filam, 4));
				cmu.getUm().getComboBoxTipo().setSelectedItem(this.getvUsuario().getTable().getValueAt(filam, 5));
				cmu.getUm().setVisible(true);

			}

			break;
		case "Eliminar":
			int fila = this.getvUsuario().getTable().getSelectedRow();
			if (fila == -1) {
				JOptionPane.showMessageDialog(this.getvUsuario(), "Debe seleccionar un usuario", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			} else {
				int i = JOptionPane.showConfirmDialog(this.getvUsuario(), "�Desea eliminar el usuario?");
				if (i == JOptionPane.YES_OPTION) {
					String dni = (String) this.getvUsuario().getTable().getValueAt(fila, 1);
					if (this.getUdi().borrarUsuario(dni)) {
						JOptionPane.showMessageDialog(this.getvUsuario(), "Usuario eliminado correctamente", "Sistema",
								JOptionPane.INFORMATION_MESSAGE);
					}
				}
			}
			break;
		
		}}
		

	@Override
	public void windowActivated(WindowEvent e) {
		this.VentanaActivada();

	}

	public void VentanaActivada() {

		String[] columnas = { "Id_usuario", "DNI", "Nombre", "Apellido", "Nombre de usuario", "Tipo" };

		DefaultTableModel modelo = new DefaultTableModel();
		modelo.setColumnIdentifiers(columnas);

		List<Usuario> usuarios = this.getUdi().buscarUsuarios();

		for (Usuario u : usuarios) {
			Object[] row = new Object[6];
			row[0] = u.getCodigoUsuario();
			row[1] = u.getDniUsuario();
			row[2] = u.getNombreUsuario();
			row[3] = u.getApellidoUsuario();
			row[4] = u.getUser();
			row[5] = u.getTipo();

			modelo.addRow(row);
		}

		this.getvUsuario().getTable().setModel(modelo);

	}

	@Override
	public void keyPressed(KeyEvent arg0) {

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		if (!this.getvUsuario().getTxtBuscar().getText().isEmpty()) {
			String cad = this.getvUsuario().getTxtBuscar().getText();
			String[] columnas = { "Id_usuario", "DNI", "Nombre", "Apellido", "Nombre de usuario", "Tipo" };
			DefaultTableModel modelo = new DefaultTableModel();
			modelo.setColumnIdentifiers(columnas);
			List<Usuario> usuarios = this.getUdi().buscarUsuario2(cad);
			for (Usuario u : usuarios) {
				Object[] row = new Object[6];
				row[0] = u.getCodigoUsuario();
				row[1] = u.getDniUsuario();
				row[2] = u.getNombreUsuario();
				row[3] = u.getApellidoUsuario();
				row[4] = u.getUser();
				row[5] = u.getTipo();
				modelo.addRow(row);

			}

			this.getvUsuario().getTable().setModel(modelo);
		} else {
			this.VentanaActivada();
		}

	}

	@Override
	public void keyTyped(KeyEvent arg0) {

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	public VistaUsuario getvUsuario() {
		return vUsuario;
	}

	public void setvUsuario(VistaUsuario vUsuario) {
		this.vUsuario = vUsuario;
	}

	public UsuarioDaoImp getUdi() {
		return udi;
	}

	public void setUdi(UsuarioDaoImp udi) {
		this.udi = udi;
	}


}
