package controlador;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import modelo.VentaDaoImpl;
import vista.VistaVenta;

public class ControladorVenta implements ActionListener, WindowListener {

	private VistaVenta vVenta;
	private VentaDaoImpl vdi = new VentaDaoImpl();
	private String usuario;

	public ControladorVenta() {

		this.setvVenta(new VistaVenta(this));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		String pelicula = "";
		ControladorVentaFuncion cvf;
		switch (btn.getText()) {

		case "Pelicula 1":
			vVenta.dispose();
			pelicula = this.getvVenta().getBtnPelicula().getName();
			cvf = new ControladorVentaFuncion();
			cvf.setCodigo_pelicula(pelicula);
			cvf.setUsuario(this.getUsuario());

			cvf.getvFuncion().setVisible(true);

			break;
		case "Pelicula 2":
			vVenta.dispose();
			pelicula = this.getvVenta().getBtnPelicula2().getName();
			cvf = new ControladorVentaFuncion();
			cvf.setCodigo_pelicula(pelicula);
			cvf.setUsuario(this.getUsuario());
			cvf.getvFuncion().setVisible(true);

			break;
		case "Pelicula 3":
			vVenta.dispose();
			pelicula = this.getvVenta().getBtnPelicula3().getName();
			cvf = new ControladorVentaFuncion();
			cvf.setCodigo_pelicula(pelicula);
			cvf.setUsuario(this.getUsuario());
			cvf.getvFuncion().setVisible(true);
			break;
		case "Pelicula 4":
			vVenta.dispose();
			pelicula = this.getvVenta().getBtnPelicula4().getName();
			cvf = new ControladorVentaFuncion();
			cvf.setCodigo_pelicula(pelicula);
			cvf.setUsuario(this.getUsuario());
			cvf.getvFuncion().setVisible(true);
			break;
		case "Pelicula 5":
			vVenta.dispose();
			pelicula = this.getvVenta().getBtnPelicula5().getName();
			cvf = new ControladorVentaFuncion();
			cvf.setCodigo_pelicula(pelicula);
			cvf.setUsuario(this.getUsuario());
			cvf.getvFuncion().setVisible(true);
			;
			break;
		case "Pelicula 6":
			vVenta.dispose();
			pelicula = this.getvVenta().getBtnPelicula6().getName();
			cvf = new ControladorVentaFuncion();
			cvf.setCodigo_pelicula(pelicula);
			cvf.setUsuario(this.getUsuario());
			cvf.getvFuncion().setVisible(true);
			break;
		}
	}

	@Override
	public void windowOpened(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowActivated(WindowEvent e) {
		this.imagenesBoton();

	}

	public void cargarBoton(String dire, JButton btn) {
		File f = new File(dire);

		Image foto = null;
		try {
			foto = ImageIO.read(f).getScaledInstance(btn.getWidth(), btn.getHeight(), Image.SCALE_DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}

		btn.setIcon(new ImageIcon(foto));
		btn.updateUI();

	}

	public void imagenesBoton() {
		DefaultTableModel modelo = new DefaultTableModel();
		List<String> peliculas = this.getVdi().consultarPeliculas();

		this.cargarBoton((System.getProperty("user.dir") + ("\\src\\imagenesPeliculas\\")) + peliculas.get(0),
				this.getvVenta().getBtnPelicula());
		this.getvVenta().getBtnPelicula().setName(this.obtenerCodigo(peliculas.get(0)));

		this.cargarBoton((System.getProperty("user.dir") + ("\\src\\imagenesPeliculas\\")) + peliculas.get(1),
				this.getvVenta().getBtnPelicula2());
		this.getvVenta().getBtnPelicula2().setName(this.obtenerCodigo(peliculas.get(1)));

		this.cargarBoton((System.getProperty("user.dir") + ("\\src\\imagenesPeliculas\\")) + peliculas.get(2),
				this.getvVenta().getBtnPelicula3());
		this.getvVenta().getBtnPelicula3().setName(this.obtenerCodigo(peliculas.get(2)));

		this.cargarBoton((System.getProperty("user.dir") + ("\\src\\imagenesPeliculas\\")) + peliculas.get(3),
				this.getvVenta().getBtnPelicula4());
		this.getvVenta().getBtnPelicula4().setName(this.obtenerCodigo(peliculas.get(3)));

		this.cargarBoton((System.getProperty("user.dir") + ("\\src\\imagenesPeliculas\\")) + peliculas.get(4),
				this.getvVenta().getBtnPelicula5());
		this.getvVenta().getBtnPelicula5().setName(this.obtenerCodigo(peliculas.get(4)));

		this.cargarBoton((System.getProperty("user.dir") + ("\\src\\imagenesPeliculas\\")) + peliculas.get(5),
				this.getvVenta().getBtnPelicula6());
		this.getvVenta().getBtnPelicula6().setName(this.obtenerCodigo(peliculas.get(5)));
	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	private String obtenerCodigo(String cad) {
		String cadenaCodigo = "";
		if (cad.contains(".")) {
			String[] parts = cad.split("\\.");
			cadenaCodigo = parts[0];
		}
		return cadenaCodigo;
	}

	public VistaVenta getvVenta() {
		return vVenta;
	}

	public void setvVenta(VistaVenta vVenta) {
		this.vVenta = vVenta;
	}

	public VentaDaoImpl getVdi() {
		return vdi;
	}

	public void setVdi(VentaDaoImpl vdi) {
		this.vdi = vdi;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

}
