package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultListModel;

import modelo.Funcion;
import modelo.VentaDaoImpl;
import vista.VentaFuncion;

public class ControladorVentaFuncion implements ActionListener, WindowListener, MouseListener {

	private VentaFuncion vFuncion;
	private VentaDaoImpl vDi = new VentaDaoImpl();
	private String codigo_pelicula;
	private String usuario;

	public ControladorVentaFuncion() {
		this.setvFuncion(new VentaFuncion(this));
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		Integer valor = this.cantidadEntradas();
		vFuncion.dispose();
		ControladorVentaSala cvs = new ControladorVentaSala();
		cvs.setLimite(valor);
		cvs.setInfoFuncion(this.obtenerInfo());
		cvs.setUsuario(this.getUsuario());
		cvs.getvSala().setVisible(true);

	}

	@Override
	public void windowOpened(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@SuppressWarnings("unchecked")
	@Override
	public void windowActivated(WindowEvent e) {
		Integer p= Integer.parseInt(this.getCodigo_pelicula());
		List<Funcion> funciones = this.getvDi().consultarFunciones(p, this.fechaActual());
		DefaultListModel<Object> modelo = new DefaultListModel<Object>();
		Double precio, descuento, valor;
		List<String> nombre = this.getvDi().consultarPeliculas(p);
		this.getvFuncion().getLblNewLabel().setText("Pelicula: " + nombre.get(0));
		for (Funcion funcion : funciones) {

			Date d = fecha(funcion.getFechaFuncion());
			String dia = descuento(d);

			precio = Double.parseDouble(funcion.getPrecio());
			descuento = Double.parseDouble(funcion.getDescuento());
			valor = precio - descuento;
			modelo.addElement(
					"Codigo: " + funcion.getCodigoF() + " - " + "Horario: " + funcion.getHorarioFuncion() + " - "
							+ "Fecha: " + obtenerDia(funcion.getFechaFuncion()) + " - " + "Sala: " + funcion.getSala()
							+ " - Precio: " + (valor) + " - Nombre: " + funcion.getNombreFuncion());
		}
		this.getvFuncion().getList().setModel(modelo);

	}

	public Integer lista() {
		int i = this.getvFuncion().getList().getSelectedIndex();
		return i;
	}

	@SuppressWarnings("deprecation")
	public String descuento(Date d) {
		String[] strDAys = new String[] { "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" };
		String dia = strDAys[d.getDay()];
		return dia;
	}

	public Date fecha(String fecha) {
		SimpleDateFormat formatodeltexto = new SimpleDateFormat("dd/MM/yyyy");
		String fechas = fecha.trim();
		Date dato = null;
		try {
			dato = (Date) formatodeltexto.parse(fechas);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dato;
	}

	public String fechaActual() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String fecha = sdf.format(date);
		return fecha;
	}

	private String obtenerDia(String cad) {
		String cadena = "";
		if (cad.contains("/")) {
			String[] parts = cad.split("/");
			cadena = parts[0] + "/" + parts[1];
		}
		return cadena;
	}

	@SuppressWarnings("unused")
	public Integer cantidadEntradas() {
		String valorSpinner = "";
		Integer cantEntradas = 0;
		cantEntradas = (Integer) this.getvFuncion().getSpinnerEntradas_1().getValue();
		return cantEntradas;
	}

	private String obtenerInfo() {

		String cadenaCodigo = (String) this.getvFuncion().getList().getSelectedValue();
		return cadenaCodigo;
	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int i = this.getvFuncion().getList().getSelectedIndex();
		if (i != -1) {
			this.getvFuncion().getBtnSiguiente().setEnabled(true);

		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public VentaFuncion getvFuncion() {
		return vFuncion;
	}

	public void setvFuncion(VentaFuncion vFuncion) {
		this.vFuncion = vFuncion;
	}

	public VentaDaoImpl getvDi() {
		return vDi;
	}

	public void setvDi(VentaDaoImpl vDi) {
		this.vDi = vDi;
	}
	public String getCodigo_pelicula() {
		return codigo_pelicula;
	}

	public void setCodigo_pelicula(String codigo_pelicula) {
		this.codigo_pelicula = codigo_pelicula;
	}
	

}
