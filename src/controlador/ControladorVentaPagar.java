package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import basededatos.BD;
import modelo.FuncionDaoImpl;
import modelo.UsuarioDaoImp;
import modelo.Venta;
import modelo.VentaDaoImpl;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import vista.VistaPagar;

public class ControladorVentaPagar implements ActionListener, WindowListener {

	private VistaPagar vp;
	private ArrayList<String> asientos = new ArrayList<>();
	private String infoFuncion;
	private FuncionDaoImpl fdi = new FuncionDaoImpl();
	private VentaDaoImpl vdi = new VentaDaoImpl();
	private String usuario;
	private UsuarioDaoImp udi = new UsuarioDaoImp();

	public ControladorVentaPagar() {
		this.setVp(new VistaPagar(this));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Integer j = this.getUdi().buscarUsuario(this.getUsuario());
		if (e.getSource() == this.getVp().getBtnConfirmarYGenerar()) {
			String CodigoFuncion = this.obtenerCodigoDosPuntos(this.obtenerCodigoGuion(0), 1);
			String CodigoSala = this.obtenerCodigoDosPuntos(this.obtenerCodigoGuion(3), 1);
			Integer cF = Integer.parseInt(CodigoFuncion);
			for (int i = 0; i < this.getAsientos().size(); i++) {

				this.getFdi().modificarFuncionAsiento(cF, this.getAsientos().get(i), CodigoSala);

			}

			String importe = this.getVp().getLblI().getText();
			Venta venta = new Venta(j, cF, this.getAsientos().toString(), importe);
			if (this.getVdi().nuevaVenta(venta)) {
				JOptionPane.showMessageDialog(this.getVp(), "Venta realizada con �xito", "Sistema",
						JOptionPane.INFORMATION_MESSAGE);
				this.getVp().dispose();

			}

		}
		try {
			this.reporteVenta();
		} catch (JRException e1) {
			e1.printStackTrace();
		}
	}

	public void reporteVenta() throws JRException {
		String ruta = System.getProperty("user.dir");
		JasperPrint jasperPrintWindow = JasperFillManager.fillReport(ruta + "\\reportes\\Tree.jasper", null,
				BD.getInstance().getConnection());
		JasperViewer jasperViewer = new JasperViewer(jasperPrintWindow, false);

		jasperViewer.setVisible(true);

	}

	@Override
	public void windowActivated(WindowEvent e) {

		this.completarCampos();

	}

	public void completarCampos() {
		Double total = 0.0;

		this.getVp().getLblP().setText(this.obtenerCodigoDosPuntos(this.obtenerCodigoGuion(5), 1));

		this.getVp().getLblH().setText(this.obtenerCodigoDosPuntos(this.obtenerCodigoGuion(1), 1) + ":"
				+ this.obtenerCodigoDosPuntos(this.obtenerCodigoGuion(1), 2));

		this.getVp().getLblS().setText(this.obtenerCodigoDosPuntos(this.obtenerCodigoGuion(3), 1));

		this.getVp().getLblF().setText(this.obtenerCodigoDosPuntos(this.obtenerCodigoGuion(2), 1));

		for (String string : asientos) {
			this.getVp().getLblB().setText(this.getVp().getLblB().getText() + string.toString() + " - ");
			String pre = this.obtenerCodigoDosPuntos(this.obtenerCodigoGuion(4), 1);

			Double precio = Double.parseDouble(pre);

			total = total + precio;
		}

		this.getVp().getLblI().setText(total.toString());

	}

	private String obtenerNumeroAsiento(String cadena) { // esto es para obtener informacion de la funcion
		String cade = "";
		String[] parts = cadena.split(":");
		cade = parts[2];

		return cade;
	}

	private String obtenerCodigoGuion(Integer i) {
		String cade = this.getInfoFuncion();

		String[] parts = cade.split("-");
		cade = parts[i];

		return cade;
	}

	private String obtenerCodigoDosPuntos(String cade, Integer j) {// esto es para obtener informacion de la funcion
		String ca;
		String[] parts = cade.split(":");
		ca = parts[j].replaceAll(" ", "");

		return ca;
	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowOpened(WindowEvent e) {

	}

	public VistaPagar getVp() {
		return vp;
	}

	public void setVp(VistaPagar vp) {
		this.vp = vp;
	}

	public ArrayList<String> getAsientos() {
		return asientos;
	}

	public void setAsientos(ArrayList<String> asientos) {
		this.asientos = asientos;
	}

	public String getInfoFuncion() {
		return infoFuncion;
	}

	public void setInfoFuncion(String infoFuncion) {
		this.infoFuncion = infoFuncion;
	}

	public FuncionDaoImpl getFdi() {
		return fdi;
	}

	public void setFdi(FuncionDaoImpl fdi) {
		this.fdi = fdi;
	}

	public VentaDaoImpl getVdi() {
		return vdi;
	}

	public void setVdi(VentaDaoImpl vdi) {
		this.vdi = vdi;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public UsuarioDaoImp getUdi() {
		return udi;
	}

	public void setUdi(UsuarioDaoImp udi) {
		this.udi = udi;
	}
}
