package controlador;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JToggleButton;
import modelo.AsientoDaoImp;
import modelo.FuncionDaoImpl;
import vista.VentaSala;

public class ControladorVentaSala implements ActionListener, MouseListener, WindowListener {

	private VentaSala vSala;
	private ControladorVentaFuncion cvf;
	private Integer limite = 0;
	private String infoFuncion;
	private AsientoDaoImp adi = new AsientoDaoImp();
	private ArrayList<String> asientos = new ArrayList<>();
	private FuncionDaoImpl cfi = new FuncionDaoImpl();
	private String usuario;

	public ControladorVentaSala() {
		this.setvSala(new VentaSala(this));

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getvSala().getBtnSiguientePagar()) {
			this.getvSala().dispose();
			ControladorVentaPagar cvp = new ControladorVentaPagar();
			cvp.setAsientos(this.getAsientos());
			cvp.setInfoFuncion(this.getInfoFuncion());
			cvp.setUsuario(this.getUsuario());
			cvp.getVp().setVisible(true);

		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
		Integer valor = 0;
		Integer seleccion = 0;
		
		Component[] componentes = this.getvSala().getPanel().getComponents();
		for (int i = 0; i < componentes.length; i++) {
			if (componentes[i] instanceof JToggleButton) {
				if (((JToggleButton) componentes[i]).isSelected() && (seleccion < this.getLimite())) {
					seleccion++;
				
				} else {
					((JToggleButton) componentes[i]).setSelected(false);
					this.getvSala().getBtnSiguientePagar().setEnabled(true);
				}
				
				if (valor <= this.getLimite()) {
					valor++;
				}	
			}
		}
		}
		
	

	@Override
	public void mouseReleased(MouseEvent e) {
		String r = "";
		Integer valor = 0;
		Integer seleccion = 0;
		Component[] componentes = this.getvSala().getPanel().getComponents();
		for (int i = 0; i < componentes.length; i++) {
			if (componentes[i] instanceof JToggleButton) {
				r = ((JToggleButton) componentes[i]).getName();
				r = this.obtenerNumeroAsiento(r);
				if (((JToggleButton) componentes[i]).isSelected() && (seleccion < this.getLimite())) {
					seleccion++;
					if (!this.getAsientos().contains(r)) {
						this.getAsientos().add(r);
					}

				} else {
					((JToggleButton) componentes[i]).setSelected(false);
					if (this.getAsientos().contains(r)) {
						this.getAsientos().remove(r);
					}
				}
				if (valor <= this.getLimite()) {
					valor++;
				}
			}
		}

	}

	private String obtenerNumeroAsiento(String cadena) { // esto es para obtener informacion de la funcion
		String cade = "";
		String[] parts = cadena.split(":");
		cade = parts[2];

		return cade;
	}

	private String obtenerCodigoGuion(Integer i) {
		String cade = this.getInfoFuncion();

		String[] parts = cade.split("-");
		cade = parts[i];

		return cade;
	}

	private String obtenerCodigoDosPuntos(String cade, Integer j) {// esto es para obtener informacion de la funcion
		String ca;
		String[] parts = cade.split(":");
		ca = parts[j].replaceAll(" ", "");

		return ca;
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void windowActivated(WindowEvent e) {
		String nombreAsiento, ocupado;
		String cF = this.obtenerCodigoDosPuntos(this.obtenerCodigoGuion(0), 1);
		List<String> asientosOcupados;
		asientosOcupados = this.getAdi().buscarButacasOcupadas(cF);
		Component[] componentes = this.vSala.getPanel().getComponents();
		for (int i = 0; i < componentes.length; i++) {
			if (componentes[i] instanceof JToggleButton) {

				nombreAsiento = ((JToggleButton) componentes[i]).getName();
				ocupado = this.obtenerCodigoDosPuntos(nombreAsiento, 2);
				if (asientosOcupados.contains(ocupado)) {
					((JToggleButton) componentes[i]).setBackground(Color.red);
					((JToggleButton) componentes[i]).setEnabled(false);
				}

			}
		}
	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowOpened(WindowEvent e) {

	}

	public VentaSala getvSala() {
		return vSala;
	}

	public void setvSala(VentaSala vSala) {
		this.vSala = vSala;
	}

	public ControladorVentaFuncion getCvf() {
		return cvf;
	}

	public void setCvf(ControladorVentaFuncion cvf) {
		this.cvf = cvf;
	}

	public Integer getLimite() {
		return limite;
	}

	public void setLimite(Integer limite) {
		this.limite = limite;
	}

	public String getInfoFuncion() {
		return infoFuncion;
	}

	public void setInfoFuncion(String infoFuncion) {
		this.infoFuncion = infoFuncion;
	}

	public AsientoDaoImp getAdi() {
		return adi;
	}

	public void setAdi(AsientoDaoImp adi) {
		this.adi = adi;
	}

	public ArrayList<String> getAsientos() {
		return asientos;
	}

	public void setAsientos(ArrayList<String> asientos) {
		this.asientos = asientos;
	}

	public FuncionDaoImpl getCfi() {
		return cfi;
	}

	public void setCfi(FuncionDaoImpl cfi) {
		this.cfi = cfi;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
