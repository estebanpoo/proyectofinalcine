package modelo;

public class Asiento {

	private String numeroAsiento;
	private String asientoOcupado;
	private String codigoSala;

	public Asiento(String numeroAsiento, String asientoOcupado) {
		super();
		this.setNumeroAsiento(numeroAsiento);
		this.setAsientoOcupado(asientoOcupado);
	}

	public Asiento() {

	}

	public Asiento(String numeroAsiento, String asientoOcupado, String codigoSala) {
		super();
		this.setNumeroAsiento(numeroAsiento);
		this.setAsientoOcupado(asientoOcupado);
		this.setCodigoSala(codigoSala);
	}

	public String getNumeroAsiento() {
		return numeroAsiento;
	}

	public void setNumeroAsiento(String numeroAsiento) {
		this.numeroAsiento = numeroAsiento;
	}

	public String getAsientoOcupado() {
		return asientoOcupado;
	}

	public void setAsientoOcupado(String asientoOcupado) {
		this.asientoOcupado = asientoOcupado;
	}

	public String getCodigoSala() {
		return codigoSala;
	}

	public void setCodigoSala(String codigoSala) {
		this.codigoSala = codigoSala;
	}

}
