package modelo;

import java.util.List;

public interface AsientoDao {

	public Boolean agregarAsiento(Asiento a);

	public List<Asiento> buscarTotalButacas(String codigo_sala);

	public List<String> buscarButacasOcupadas(String codigo_sala);

	public Boolean actualizarButacas(Asiento butaca);

}
