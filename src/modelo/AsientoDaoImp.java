package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import basededatos.BD;

public class AsientoDaoImp implements AsientoDao {

	@Override
	public Boolean agregarAsiento(Asiento a) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(a.getNumeroAsiento());
		parametros.add(a.getAsientoOcupado());
		parametros.add(a.getCodigoSala());
		String consulta = "insert into asiento (numero_asiento, disponible,codigo_sala)" + "values(?,?,?)";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public List<Asiento> buscarTotalButacas(String codigo_sala) {
		String consulta = "select * from asiento where codigo_sala='" + codigo_sala + "' order by numero_asiento";
		List<Asiento> a = new ArrayList<Asiento>();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Asiento as;
				as = new Asiento(rs.getString("numero_asiento"), rs.getString("disponible"),
						rs.getString("codigo_sala"));
				a.add(as);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return a;
	}

	@Override
	public List<String> buscarButacasOcupadas(String codigo) {
		String consulta = "select * from funcion_asiento where codigo_funcion=" + "'" + codigo + "'"
				+ " and disponible='no'";
		List<String> asientosOcupados = new ArrayList<String>();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				String cade = rs.getString("codigo_asiento");
				asientosOcupados.add(cade);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return asientosOcupados;
	}

	
	@Override
	public Boolean actualizarButacas(Asiento butaca) {
		return null;
	}
}
