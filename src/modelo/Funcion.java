package modelo;

public class Funcion {

	private Integer codigoF;
	private Integer pelicula;
	private String nombreFuncion;
	private String fechaFuncion;
	private String horarioFuncion;

	private String sala;
	private String precio;
	private String descuento;
	private String estado;

	public Funcion() {

	}

	/*
	 * public Funcion(String codigoFuncion, String pelicula, String nombreFuncion,
	 * String sala, String fechaFuncion, String horarioFuncion, String precio,
	 * String descuento) { this.setCodigoFuncion(codigoFuncion);
	 * 
	 * this.setPelicula(pelicula); this.setNombreFuncion(nombreFuncion);
	 * this.setSala(sala); this.setFechaFuncion(fechaFuncion);
	 * this.setHorarioFuncion(horarioFuncion); this.setPrecio(precio);
	 * this.setDescuento(descuento); }
	 */
	public Funcion(Integer codigo_funcion, Integer pelicula, String nombreFuncion, String sala, String fechaFuncion,
			String horarioFuncion, String precio, String descuento) {
		this.setCodigoF(codigo_funcion);
		this.setPelicula(pelicula);
		this.setNombreFuncion(nombreFuncion);
		this.setSala(sala);
		this.setFechaFuncion(fechaFuncion);
		this.setHorarioFuncion(horarioFuncion);
		this.setPrecio(precio);
		this.setDescuento(descuento);
		this.setEstado("Activo");
	}

	public Funcion(Integer pelicula, String nombreFuncion, String sala, String fechaFuncion, String horarioFuncion,
			String precio, String descuento) {

		this.setPelicula(pelicula);
		this.setNombreFuncion(nombreFuncion);
		this.setSala(sala);
		this.setFechaFuncion(fechaFuncion);
		this.setHorarioFuncion(horarioFuncion);
		this.setPrecio(precio);
		this.setDescuento(descuento);
	}

	public String getSala() {
		return sala;
	}

	public Integer getPelicula() {
		return pelicula;
	}

	public void setPelicula(Integer pelicula) {
		this.pelicula = pelicula;
	}

	public void setSala(String sala) {
		this.sala = sala;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getNombreFuncion() {
		return nombreFuncion;
	}

	public void setNombreFuncion(String nombreFuncion) {
		this.nombreFuncion = nombreFuncion;
	}

	public String getFechaFuncion() {
		return fechaFuncion;
	}

	public void setFechaFuncion(String fechaFuncion) {
		this.fechaFuncion = fechaFuncion;
	}

	public String getHorarioFuncion() {
		return horarioFuncion;
	}

	public void setHorarioFuncion(String horarioFuncion) {
		this.horarioFuncion = horarioFuncion;
	}

	public String getDescuento() {
		return descuento;
	}

	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getCodigoF() {
		return codigoF;
	}

	public void setCodigoF(Integer codigoF) {
		this.codigoF = codigoF;
	}

}