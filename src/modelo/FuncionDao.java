package modelo;

import java.util.List;

import javax.swing.JComboBox;

public interface FuncionDao {

	public Boolean agregarFuncion(Funcion funcion);

	public List<Funcion> buscarFunciones();

	public Boolean borrarFuncion(Integer codigo);

	public Boolean modificarFuncion(Funcion funcion);

	public void consultarPeliculas(JComboBox<String> combo_pelicula);

	public void consultarSalas(JComboBox<String> combo_sala);

	public List<Funcion> buscarFuncion2(String cade);

}
