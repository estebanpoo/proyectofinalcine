package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;

import basededatos.BD;

public class FuncionDaoImpl implements FuncionDao{

	@Override
	public Boolean agregarFuncion(Funcion funcion) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(funcion.getPelicula());
		parametros.add(funcion.getNombreFuncion());
		parametros.add(funcion.getSala());
		parametros.add(funcion.getFechaFuncion());
		parametros.add(funcion.getHorarioFuncion());
		parametros.add(funcion.getPrecio());
		parametros.add(funcion.getDescuento());
		parametros.add("Activo");
		
		String consulta="insert into funcion(codigo_pelicula,nombre_funcion,codigo_sala,fecha_funcion,horario_funcion,precio,descuento,estado) "+" values(?,?,?,?,?,?,?,?)";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public List<Funcion> buscarFunciones() {
		String consulta = "select * from funcion where estado='Activo' order by codigo desc";
		List<Funcion>funcion= new ArrayList<Funcion>();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while(rs.next())
			{
				Funcion f;
				f=new Funcion(rs.getInt("codigo"),rs.getInt("codigo_pelicula"),rs.getString("nombre_funcion"),rs.getString("codigo_sala"),rs.getString("fecha_funcion"),rs.getString("horario_funcion"),rs.getString("precio"),rs.getString("descuento"));
				funcion.add(f);
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return funcion;
	}

	@Override
	public Boolean borrarFuncion(Integer codigo) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(codigo);

		String consulta = "update funcion set estado ='Inactivo' where codigo=?";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean modificarFuncion(Funcion funcion) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		
		parametros.add(funcion.getPelicula());
		parametros.add(funcion.getNombreFuncion());
		parametros.add(funcion.getSala());
		parametros.add(funcion.getFechaFuncion());
		parametros.add(funcion.getHorarioFuncion());
		parametros.add(funcion.getPrecio());
		parametros.add(funcion.getDescuento());
		
		parametros.add(funcion.getCodigoF());
		String consulta="update funcion set codigo_pelicula=?,nombre_funcion=?,codigo_sala=?,fecha_funcion=?,horario_funcion=?,precio=?,descuento=? where codigo= ?";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public void consultarPeliculas(JComboBox<String> combo_pelicula) {
		String consulta = "select codigo,nombre from pelicula order by codigo desc";
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while(rs.next())
			{
				combo_pelicula.addItem(rs.getString("codigo")+"-"+rs.getString("nombre"));		
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return ;
		
	}
	public String consultarPeliculas(Integer pelicula) {
		String consulta = "select * from pelicula where codigo=" + pelicula;
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		String peli="";
		try {
			while(rs.next())
			{
				peli= rs.getString("codigo")+"-"+rs.getString("nombre");		
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return peli ;
		
	}

	@Override
	public void consultarSalas(JComboBox<String> combo_sala) {
		String consulta = "select codigo_sala,nombre_sala from sala order by codigo_sala";
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while(rs.next())
			{
				combo_sala.addItem(rs.getString("codigo_sala")+"-"+rs.getString("nombre_sala"));		
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return ;
		
	}
	public String consultarSalas(String codigo_sala) {
		String consulta = "select * from sala where codigo_sala="+ "'"+codigo_sala+"'";
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		String sala="";
		try {
			while(rs.next())
			{
				 sala= rs.getString("codigo_sala")+"-"+rs.getString("nombre_sala");	
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return sala;
		
	}

	@Override
	public List<Funcion> buscarFuncion2(String cade) {

		String consulta = "select * from funcion where (estado= 'Activo') and (nombre_funcion like'%" + cade+ "%'"+"or codigo::text like '%"+cade+"%'"
				+ "or codigo_pelicula::text like '%"+cade+"%'"+
			"or codigo_sala like '%" + cade + "%'" + "or fecha_funcion like '%" + cade + "%'" + "or horario_funcion like '%" + cade + "%')";
		List<Funcion> fun = new ArrayList<Funcion>();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Funcion f;

				f=new Funcion(rs.getInt("codigo"),rs.getInt("codigo_pelicula"),rs.getString("nombre_funcion"),rs.getString("codigo_sala"),rs.getString("fecha_funcion"),rs.getString("horario_funcion"),rs.getString("precio"),rs.getString("descuento"));
				fun.add(f);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return fun;
	}
	
	public Boolean agregarFuncionAsiento(Funcion f, Asiento a)
	{
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(f.getCodigoF());
		parametros.add(a.getNumeroAsiento());
		parametros.add(a.getCodigoSala());
		parametros.add("si");
		String consulta="insert into funcion_asiento(codigo_funcion, codigo_asiento,codigo_sala ,disponible) "+" values(?,?,?,?)";
		return BD.getInstance().manipularEntidades(consulta, parametros);
		
	}
	public Boolean modificarFuncionAsiento(Integer cF, String cA, String cS) {
	ArrayList<Object> parametros = new ArrayList<Object>();
	
	
	parametros.add(cF);
	parametros.add(cA);
	parametros.add(cS);	
	
		String consulta="update funcion_asiento set disponible='no' where codigo_funcion=? and codigo_asiento=? and codigo_sala=?";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}
	public Funcion buscarUltima() {
		
		
		String consulta = "select * from funcion order by codigo desc limit 1";
		Funcion f=null;
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				

				f=new Funcion(rs.getInt("codigo"),rs.getInt("codigo_pelicula"),rs.getString("nombre_funcion"),rs.getString("codigo_sala"),rs.getString("fecha_funcion"),rs.getString("horario_funcion"),rs.getString("precio"),rs.getString("descuento"));
				
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return f;
	}
	
	public String consultarDescripcion(Integer pelicula) {
		String consulta = "select * from pelicula where codigo=" + pelicula;
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		String peli="";
		try {
			while(rs.next())
			{
				peli= rs.getString("descripcion");		
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return peli ;
		
	}
	
	
}
