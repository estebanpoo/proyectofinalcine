package modelo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Pelicula {

	private Integer id_pelicula;
	private String nombrePelicula;
	private String audioDoble;
	private String generoPelicula;
	private String puntajeCritica;
	private String duracionPelicula;
	private String condicionPelicula;
	private String imagenPelicula;
	private String estado;
	private String fecha;
	private String descripcion;

	// ALTA
	public Pelicula(String nombrePelicula, String audioDoble, String generoPelicula, String puntajeCritica,
			String duracionPelicula, String condicionPelicula, String imagenPelicula, String estado,
			String descripcion) {

		this.setNombrePelicula(nombrePelicula);
		this.setAudioDoble(audioDoble);
		this.setGeneroPelicula(generoPelicula);
		this.setPuntajeCritica(puntajeCritica);
		this.setDuracionPelicula(duracionPelicula);
		this.setCondicionPelicula(condicionPelicula);
		this.setImagenPelicula(imagenPelicula);
		this.setEstado(estado);

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		this.setFecha(sdf.format(date));
		this.setDescripcion(descripcion);
	}

	// modificar y buscar
	public Pelicula(Integer codigoPelicula, String nombrePelicula, String audioDoble, String generoPelicula,
			String puntajeCritica, String duracionPelicula, String condicionPelicula, String imagenPelicula,
			String estado, String fecha, String des) {
		this.setId_pelicula(codigoPelicula);
		this.setNombrePelicula(nombrePelicula);
		this.setAudioDoble(audioDoble);
		this.setGeneroPelicula(generoPelicula);
		this.setPuntajeCritica(puntajeCritica);
		this.setDuracionPelicula(duracionPelicula);
		this.setCondicionPelicula(condicionPelicula);
		this.setImagenPelicula(imagenPelicula);
		this.setEstado(estado);

		this.setFecha(fecha);
		this.setDescripcion(des);
	}

	public Pelicula(Integer codigoPelicula, String nombrePelicula, String audioDoble, String generoPelicula,
			String puntajeCritica, String duracionPelicula, String condicionPelicula, String imagenPelicula,
			String des) {
		this.setId_pelicula(codigoPelicula);
		this.setNombrePelicula(nombrePelicula);
		this.setAudioDoble(audioDoble);
		this.setGeneroPelicula(generoPelicula);
		this.setPuntajeCritica(puntajeCritica);
		this.setDuracionPelicula(duracionPelicula);
		this.setCondicionPelicula(condicionPelicula);
		this.setImagenPelicula(imagenPelicula);
		this.setDescripcion(des);

	}

	public String getNombrePelicula() {
		return nombrePelicula;
	}

	public void setNombrePelicula(String nombrePelicula) {
		this.nombrePelicula = nombrePelicula;
	}

	public String getAudioDoble() {
		return audioDoble;
	}

	public void setAudioDoble(String audioDoble) {
		this.audioDoble = audioDoble;
	}

	public String getGeneroPelicula() {
		return generoPelicula;
	}

	public void setGeneroPelicula(String generoPelicula) {
		this.generoPelicula = generoPelicula;
	}

	public String getPuntajeCritica() {
		return puntajeCritica;
	}

	public void setPuntajeCritica(String puntajeCritica) {
		this.puntajeCritica = puntajeCritica;
	}

	public String getDuracionPelicula() {
		return duracionPelicula;
	}

	public void setDuracionPelicula(String duracionPelicula) {
		this.duracionPelicula = duracionPelicula;
	}

	public String getCondicionPelicula() {
		return condicionPelicula;
	}

	public void setCondicionPelicula(String condicionPelicula) {
		this.condicionPelicula = condicionPelicula;
	}

	public String getImagenPelicula() {
		return imagenPelicula;
	}

	public void setImagenPelicula(String imagenPelicula) {
		this.imagenPelicula = imagenPelicula;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Integer getId_pelicula() {
		return id_pelicula;
	}

	public void setId_pelicula(Integer id_pelicula) {
		this.id_pelicula = id_pelicula;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
