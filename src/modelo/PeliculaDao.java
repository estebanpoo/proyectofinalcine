package modelo;

import java.util.List;

public interface PeliculaDao {

	public Boolean agregarPelicula(Pelicula p);

	public List<Pelicula> buscarPeliculas();

	public Boolean borrarPelicula(Integer codigo);

	public Boolean modificarPelicula(Pelicula p);

	public List<Pelicula> buscarPeliculas2(String cade);
	
	public Integer buscarUltimoCodigo();
}
