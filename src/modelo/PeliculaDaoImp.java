package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import basededatos.BD;

public class PeliculaDaoImp implements PeliculaDao {

	@Override
	public Boolean agregarPelicula(Pelicula p) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		
		parametros.add(p.getNombrePelicula());
		parametros.add(p.getAudioDoble());
		parametros.add(p.getGeneroPelicula());
		parametros.add(p.getPuntajeCritica());
		parametros.add(p.getDuracionPelicula());
		parametros.add(p.getCondicionPelicula());
		parametros.add(p.getImagenPelicula());
		parametros.add(p.getEstado());
		parametros.add(p.getFecha());
		parametros.add(p.getDescripcion());

		String consulta = "insert into pelicula(nombre,audio_doble,genero,puntaje,duracion,condicion,imagen,estado,fecha_alta,descripcion) "
				+ " values(?,?,?,?,?,?,?,?,?,?)";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public List<Pelicula> buscarPeliculas() {
		String consulta = "select * from pelicula where estado ='Activo' order by codigo desc";
		List<Pelicula> peli = new ArrayList<Pelicula>();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Pelicula p;

				p = new Pelicula(rs.getInt("codigo"), rs.getString("nombre"), rs.getString("audio_doble"),
						rs.getString("genero"), rs.getString("puntaje"), rs.getString("duracion"),
						rs.getString("condicion"), rs.getString("imagen"), rs.getString("estado"),
						rs.getString("fecha_alta"), rs.getString("descripcion"));
				peli.add(p);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return peli;
	}
	
	@Override
	public List<Pelicula> buscarPeliculas2(String cade) {

		String consulta = "select * from pelicula where estado= 'Activo' and(nombre like '%"+cade+"%'"+"or codigo::text like '%"+cade+"%'"+"or audio_doble like '%"+cade+"%'"+
		"or genero like '%"+cade+"%'"+"or puntaje like '%"+cade+"%'"+"or duracion like '%"+cade+"%'"+"or condicion like '%"+cade+"%'"+"or fecha_alta like '%"+cade+"%')";
		List<Pelicula> peli = new ArrayList<Pelicula>();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				 Pelicula p = new Pelicula(rs.getInt("codigo"), rs.getString("nombre"), rs.getString("audio_doble"),
						rs.getString("genero"), rs.getString("puntaje"), rs.getString("duracion"),
						rs.getString("condicion"), rs.getString("imagen"), rs.getString("estado"),
						rs.getString("fecha_alta"),rs.getString("descripcion"));
				peli.add(p);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return peli;
	}

	@Override
	public Boolean borrarPelicula(Integer codigo) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(codigo);

		String consulta = "update pelicula set estado ='Inactivo' where codigo=?";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean modificarPelicula(Pelicula p) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(p.getNombrePelicula());
		parametros.add(p.getAudioDoble());
		parametros.add(p.getGeneroPelicula());
		parametros.add(p.getPuntajeCritica());
		parametros.add(p.getDuracionPelicula());
		parametros.add(p.getCondicionPelicula());
		parametros.add(p.getImagenPelicula());
		parametros.add(p.getDescripcion());
		parametros.add(p.getId_pelicula());

		String consulta = "update pelicula set nombre=?, audio_doble=?, genero=?, puntaje=?, duracion=?, condicion=?, imagen=?,descripcion=? where codigo= ?";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}
	
	@Override
	public Integer buscarUltimoCodigo() {
		
		
		String consulta = "select codigo from pelicula order by codigo desc limit 1";
		Integer i = 1;
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				

				i=rs.getInt("codigo");
				
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return i +1;
	}
		
	}

