package modelo;

import java.util.ArrayList;

public class Sala {

	private String codigoSala;
	private String nombreSala;
	private ArrayList<Asiento> asientos;

	public Sala(String codigoSala, String nombreSala) {
		super();
		this.setCodigoSala(codigoSala);
		this.setNombreSala(nombreSala);
		this.setAsientos(new ArrayList<Asiento>());
	}

	public String getCodigoSala() {
		return codigoSala;
	}

	public void setCodigoSala(String codigoSala) {
		this.codigoSala = codigoSala;
	}

	public String getNombreSala() {
		return nombreSala;
	}

	public void setNombreSala(String nombreSala) {
		this.nombreSala = nombreSala;
	}

	public ArrayList<Asiento> getAsientos() {
		return asientos;
	}

	public void setAsientos(ArrayList<Asiento> asientos) {
		this.asientos = asientos;
	}

}
