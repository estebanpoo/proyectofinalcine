package modelo;

import java.util.List;

public interface SalaDao {

	public Boolean agregarSala(Sala s);

	public List<Sala> buscarSala();

	public Boolean borrarSala(String codigo);

	public Boolean modificarSala(Sala s);

}
