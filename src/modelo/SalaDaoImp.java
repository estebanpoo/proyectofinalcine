package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import basededatos.BD;

public class SalaDaoImp implements SalaDao {

	@Override
	public Boolean agregarSala(Sala s) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(s.getCodigoSala());
		parametros.add(s.getNombreSala());

		String consulta = "insert into sala(codigo_sala,nombre_sala)" + "values(?,?)";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public List<Sala> buscarSala() {
		String consulta = "select * from sala";
		List<Sala> salas = new ArrayList<Sala>();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Sala s;
				s = new Sala(rs.getString("codigo_sala"), rs.getString("nombre_sala"));
				salas.add(s);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return salas;
	}

	@Override
	public Boolean borrarSala(String codigo) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(codigo);

		String consulta = "delete from sala where codigo_sala= ?";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean modificarSala(Sala s) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(s.getCodigoSala());
		parametros.add(s.getNombreSala());

		String consulta = "update sala set codigo_sala=?, nombre_sala=?, asientos=?";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}

}
