package modelo;

public class Usuario {
	private Integer codigoUsuario;
	private String dniUsuario;
	private String nombreUsuario;
	private String apellidoUsuario;
	private String user;
	private String estado;
	private String tipo;

	public Usuario() {

	}

	public Usuario(String dniUsuario, String nombreUsuario, String apellidoUsuario, String user,String tipo) {

		this.setDniUsuario(dniUsuario);
		this.setNombreUsuario(nombreUsuario);
		this.setApellidoUsuario(apellidoUsuario);
		this.setUser(user);
		this.setTipo(tipo);
	}
	public Usuario(Integer codigo,String dniUsuario, String nombreUsuario, String apellidoUsuario, String user,String tipo) {
		this.setCodigoUsuario(codigo);
		this.setDniUsuario(dniUsuario);
		this.setNombreUsuario(nombreUsuario);
		this.setApellidoUsuario(apellidoUsuario);
		this.setUser(user);
		this.setTipo(tipo);
	}
	public Usuario(Integer codigo,String dniUsuario, String nombreUsuario, String apellidoUsuario, String user) {
		this.setCodigoUsuario(codigo);
		this.setDniUsuario(dniUsuario);
		this.setNombreUsuario(nombreUsuario);
		this.setApellidoUsuario(apellidoUsuario);
		this.setUser(user);
		this.setEstado("Activo");
	}

	public String getDniUsuario() {
		return dniUsuario;
	}

	public void setDniUsuario(String dniUsuario) {
		this.dniUsuario = dniUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getApellidoUsuario() {
		return apellidoUsuario;
	}

	public void setApellidoUsuario(String apellidoUsuario) {
		this.apellidoUsuario = apellidoUsuario;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
