package modelo;

import java.util.List;

import javax.swing.JComboBox;

public interface UsuarioDao {

	public Boolean agregarUsuario(Usuario u);

	public List<Usuario> buscarUsuarios();

	public Boolean borrarUsuario(String dni);

	public Boolean modificarUsuario(Usuario u);

	public Usuario validarUsuario(String dni, String user);

	public void consultarUsuario(JComboBox<String> comboUsuario);

	public Integer buscarUsuario(String dni);

	public List<Usuario> buscarUsuario2(String cade);
}
