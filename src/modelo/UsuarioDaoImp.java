package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import basededatos.BD;

public class UsuarioDaoImp implements UsuarioDao {

	@Override
	public Boolean agregarUsuario(Usuario u) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(u.getDniUsuario());
		parametros.add(u.getNombreUsuario());
		parametros.add(u.getApellidoUsuario());
		parametros.add(u.getUser());
		parametros.add(u.getEstado());
		String consulta = "insert into usuario(dni,nombre,apellido,nombre_usuario,estado) " + " values(?,?,?,?,?)";
		return BD.getInstance().manipularEntidades(consulta, parametros);

	}

	@Override
	public List<Usuario> buscarUsuarios() {

		String consulta = "select * from usuario where estado ='Activo' order by id_usuario";
		List<Usuario> usu = new ArrayList<Usuario>();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Usuario u;

				u = new Usuario(rs.getInt("id_usuario"),rs.getString("dni"), rs.getString("nombre"), rs.getString("apellido"),
						rs.getString("nombre_usuario"),rs.getString("tipo"));
				usu.add(u);
			}
		} catch (SQLException e) {

			JOptionPane.showMessageDialog(null, "Problemas con la base de datos", "Sistema", JOptionPane.ERROR_MESSAGE);		}

		return usu;
	}
	
	
	public Integer buscarUsuario (String dni) {

		String consulta = "select id_usuario from usuario where dni="+"'"+dni+"'";
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		Integer i=0;
		try {
			while (rs.next()) {
				
				i = rs.getInt("id_usuario");
			}
		} catch (SQLException e) {

			JOptionPane.showMessageDialog(null, "Problemas con la base de datos", "Sistema", JOptionPane.ERROR_MESSAGE);		}

		return i;
	}
	
	
	public List<Usuario> buscarUsuario2(String cade){
		String consulta="select * from usuario where (dni like '%"+cade+"%'"+"or nombre like '%"+cade+"%'"+"or apellido like '%"+cade+"%'"+
	"or nombre_usuario like '%"+cade+"%'"+"or id_usuario ::text like '%" + cade+"%'" + "or tipo like '%" + cade + "%')";
		List<Usuario> usu= new ArrayList<Usuario>();
		ResultSet rs= BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Usuario u;
				u=new Usuario(rs.getInt("id_usuario"),rs.getString("dni"),rs.getString("nombre"), rs.getString("apellido"), rs.getString("nombre_usuario"),rs.getString("tipo"));
				usu.add(u);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Problemas con la base de datos", "Sistema", JOptionPane.ERROR_MESSAGE);		}
		
		return usu;
		
	}
	

	@Override
	public Boolean borrarUsuario(String dni) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(dni);

		String consulta = "update usuario set estado ='Inactivo' where dni=?";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean modificarUsuario(Usuario u) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		
		parametros.add(u.getNombreUsuario());
		parametros.add(u.getApellidoUsuario());
		parametros.add(u.getUser());
		parametros.add(u.getDniUsuario());
		parametros.add(u.getCodigoUsuario());
		String consulta = "update usuario set nombre=?, apellido=?, nombre_usuario=?, dni=? where id_usuario= ?";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Usuario validarUsuario(String dni, String user) {
		String consulta = "select * from usuario where dni=" + "'" + dni + "'" + "and nombre_usuario= " + "'" + user
				+ "'";
		;
		Usuario u = new Usuario();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {

				u = new Usuario(rs.getString("dni"), rs.getString("nombre"), rs.getString("apellido"),
						rs.getString("nombre_usuario"), rs.getString("tipo"));
			}
		} catch (SQLException e) {

			JOptionPane.showMessageDialog(null, "Problemas con la base de datos", "Sistema", JOptionPane.ERROR_MESSAGE);
		}

		return u;
	}

	@Override
	public void consultarUsuario(JComboBox<String> comboUsuario) {
		String consulta = "select id_usuario,nombre,apellido from usuario order by id_usuario";
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while(rs.next())
			{
				comboUsuario.addItem(rs.getInt("id_usuario")+"-"+rs.getString("nombre")+"-"+rs.getString("apellido"));		
			}
		}
		catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Problemas con la base de datos", "Sistema", JOptionPane.ERROR_MESSAGE);		}
		return ;
		
	}
	
	
}
