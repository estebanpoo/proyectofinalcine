package modelo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Venta {

	private Integer usuario;
	private Integer funcion;
	private String butacas;
	private String importe;
	private String fecha_emision;
	private String hora_emision;

	public Venta() {

	}

	public Venta(Integer usuario, Integer funcion, String butacas, String importe) {
		this.setUsuario(usuario);
		this.setFuncion(funcion);
		this.setButacas(butacas);
		this.setImporte(importe);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		this.setFecha_emision(sdf.format(date));
		DateFormat dateFormat = new SimpleDateFormat("HH:mm");
		Date date2 = new Date();
		this.setHora_emision(dateFormat.format(date2));

	}

	public Integer getUsuario() {
		return usuario;
	}

	public void setUsuario(Integer usuario) {
		this.usuario = usuario;
	}

	public Integer getFuncion() {
		return funcion;
	}

	public void setFuncion(Integer funcion) {
		this.funcion = funcion;
	}

	public String getButacas() {
		return butacas;
	}

	public void setButacas(String butacas) {
		this.butacas = butacas;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getFecha_emision() {
		return fecha_emision;
	}

	public void setFecha_emision(String fecha_emision) {
		this.fecha_emision = fecha_emision;
	}

	public String getHora_emision() {
		return hora_emision;
	}

	public void setHora_emision(String hora_emision) {
		this.hora_emision = hora_emision;
	}

}
