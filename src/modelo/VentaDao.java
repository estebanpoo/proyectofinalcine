package modelo;

import java.util.List;
import javax.swing.JComboBox;

public interface VentaDao {

	public Boolean nuevaVenta(Venta venta);

	public List<Venta> buscarVentas();

	public void consultarSalas(JComboBox<String> combo_sala);

	public List<String> consultarPeliculas();

	public List<Funcion> consultarFunciones(Integer codigo_p, String fecha);

}
