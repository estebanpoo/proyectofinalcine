package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;

import basededatos.BD;

public class VentaDaoImpl implements VentaDao {

	@Override
	public Boolean nuevaVenta(Venta entrada) {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(entrada.getUsuario());
		parametros.add(entrada.getFuncion());
		parametros.add(entrada.getFecha_emision());
		parametros.add(entrada.getHora_emision());
		parametros.add(entrada.getButacas());
		parametros.add(entrada.getImporte());
		String consulta = "insert into venta(id_usuario,cod_funcion,fecha_emision,hora_emision,butacas,importe_total)"
				+ "values(?,?,?,?,?,?)";
		return BD.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public List<Venta> buscarVentas() {
		String consulta = "select * from venta order by codigo_venta";
		List<Venta> entrada = new ArrayList<Venta>();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Venta e;
				e = new Venta();
				entrada.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entrada;
	}

	@Override
	public void consultarSalas(JComboBox<String> combo_sala) {
		String consulta = "select codigo_sala,nombre_sala from sala order by codigo_sala";
		combo_sala.addItem("<Seleccione una opcion>");
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				combo_sala.addItem(rs.getString("codigo_sala") + "-" + rs.getString("nombre_sala"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return;
	}

	@Override
	public List<Funcion> consultarFunciones(Integer codigo_p, String fecha) {
		String consulta = "select * from funcion where codigo_pelicula=" + "'" + codigo_p + "'" + "and fecha_funcion>="
				+ "'" + fecha + "'";
		List<Funcion> funcion = new ArrayList<Funcion>();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Funcion f;
				f = new Funcion(rs.getInt("codigo"), rs.getInt("codigo_pelicula"), rs.getString("nombre_funcion"),
						rs.getString("codigo_sala"), rs.getString("fecha_funcion"), rs.getString("horario_funcion"),
						rs.getString("precio"), rs.getString("descuento"));
				funcion.add(f);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return funcion;
	}

	@Override
	public List<String> consultarPeliculas() {
		String consulta = "select imagen from pelicula order by codigo desc";
		List<String> ruta = new ArrayList<String>();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				String cadena;
				cadena = new String(rs.getString("imagen"));
				ruta.add(cadena);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ruta;
	}

	public List<String> consultarPeliculas(Integer codigo) {
		String consulta = "select nombre from pelicula where codigo=" + "'" + codigo + "'";
		List<String> ruta = new ArrayList<String>();
		ResultSet rs = BD.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				String cadena;
				cadena = new String(rs.getString("nombre"));
				ruta.add(cadena);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ruta;
	}

}
