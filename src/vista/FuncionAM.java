package vista;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import java.util.Date;
import javax.swing.SpinnerNumberModel;
import com.toedter.calendar.JDateChooser;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Toolkit;
import javax.swing.JTextArea;

public abstract class FuncionAM extends JDialog {
	private JTextField txtFieldCodigoFuncion;
	private JLabel lblCodigo;
	private JLabel lblPelicula;
	private JLabel lblHorario;
	private JLabel lblSala;
	private JTextField txtFieldPrecio;
	private JComboBox cmboBxSala;
	private JComboBox cmboBxPelicula;
	private JButton btnGuardar;
	private JButton btnCancelar;
	private JTextField textFieldNombreFuncion;
	private JSpinner spinerHora;
	private JSpinner spinerMin;
	private JLabel lblNewLabel;
	private JDateChooser dateChooser;
	private JLabel lblDescripcion;
	private JLabel lblImagen;
	private JTextArea textAreaDescripcion;

	public FuncionAM() {
		setIconImage(
				Toolkit.getDefaultToolkit().getImage(FuncionAM.class.getResource("/imagenesPeliculas/funciones.jpg")));
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setResizable(false);
		setModal(true);
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(null);
		setLocationRelativeTo(null);

		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd/MM/yyyy");
		dateChooser.setBounds(135, 248, 95, 22);

		Date fechaActual = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String fechaSistema = formato.format(fechaActual);
		getContentPane().add(dateChooser);

		try {
			java.util.Date dato = formato.parse(fechaSistema);
			dateChooser.setMinSelectableDate(dato);
			String fecha = "";
			dato = formato.parse(fecha);
			dateChooser.setDate(dato);

		} catch (Exception e) {
		}

		lblCodigo = new JLabel("C\u00F3digo");
		lblCodigo.setForeground(Color.BLACK);
		lblCodigo.setFont(new Font("Arial", Font.PLAIN, 15));
		lblCodigo.setHorizontalAlignment(SwingConstants.LEFT);
		lblCodigo.setBounds(25, 50, 80, 20);
		getContentPane().add(lblCodigo);

		txtFieldCodigoFuncion = new JTextField();
		txtFieldCodigoFuncion.setEditable(false);
		txtFieldCodigoFuncion.setFont(new Font("Arial", Font.PLAIN, 15));
		txtFieldCodigoFuncion.setBounds(135, 50, 117, 20);
		getContentPane().add(txtFieldCodigoFuncion);
		txtFieldCodigoFuncion.setColumns(10);

		lblPelicula = new JLabel("Pel\u00EDcula*");
		lblPelicula.setForeground(Color.BLACK);
		lblPelicula.setFont(new Font("Arial", Font.PLAIN, 15));
		lblPelicula.setHorizontalAlignment(SwingConstants.LEFT);
		lblPelicula.setBounds(25, 100, 80, 20);
		getContentPane().add(lblPelicula);

		lblHorario = new JLabel("Horario*");
		lblHorario.setForeground(Color.BLACK);
		lblHorario.setFont(new Font("Arial", Font.PLAIN, 15));
		lblHorario.setHorizontalAlignment(SwingConstants.LEFT);
		lblHorario.setBounds(25, 300, 80, 27);
		getContentPane().add(lblHorario);

		lblSala = new JLabel("Sala*");
		lblSala.setForeground(Color.BLACK);
		lblSala.setFont(new Font("Arial", Font.PLAIN, 15));
		lblSala.setHorizontalAlignment(SwingConstants.LEFT);
		lblSala.setBounds(25, 200, 62, 17);
		getContentPane().add(lblSala);

		JLabel lblPrecio = new JLabel("Precio ($)*");
		lblPrecio.setForeground(Color.BLACK);
		lblPrecio.setFont(new Font("Arial", Font.PLAIN, 15));
		lblPrecio.setHorizontalAlignment(SwingConstants.LEFT);
		lblPrecio.setBounds(25, 350, 80, 17);
		getContentPane().add(lblPrecio);

		txtFieldPrecio = new JTextField();
		txtFieldPrecio.setFont(new Font("Arial", Font.PLAIN, 15));
		txtFieldPrecio.setBounds(135, 350, 117, 20);
		getContentPane().add(txtFieldPrecio);
		txtFieldPrecio.setColumns(10);

		cmboBxSala = new JComboBox();
		cmboBxSala.setFont(new Font("Arial", Font.PLAIN, 15));
		cmboBxSala.setMaximumRowCount(5);
		cmboBxSala.setBounds(135, 200, 193, 20);
		getContentPane().add(cmboBxSala);

		cmboBxPelicula = new JComboBox();
		cmboBxPelicula.setMaximumRowCount(5);
		cmboBxPelicula.setFont(new Font("Arial", Font.PLAIN, 15));
		cmboBxPelicula.setBounds(135, 100, 193, 20);
		getContentPane().add(cmboBxPelicula);

		btnGuardar = new JButton("Guardar");
		btnGuardar.setForeground(Color.BLACK);
		btnGuardar.setBackground(SystemColor.activeCaption);
		btnGuardar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnGuardar.setBounds(300, 400, 117, 27);
		getContentPane().add(btnGuardar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBackground(SystemColor.activeCaption);
		btnCancelar.setForeground(Color.BLACK);
		btnCancelar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnCancelar.setBounds(300, 475, 117, 27);
		getContentPane().add(btnCancelar);

		JLabel lblFecha = new JLabel("Fecha*");
		lblFecha.setForeground(Color.BLACK);
		lblFecha.setFont(new Font("Arial", Font.PLAIN, 15));
		lblFecha.setBounds(25, 250, 62, 20);
		getContentPane().add(lblFecha);

		JLabel lblNombreFuncion = new JLabel("Nombre*");
		lblNombreFuncion.setForeground(Color.BLACK);
		lblNombreFuncion.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNombreFuncion.setBounds(25, 150, 80, 20);
		getContentPane().add(lblNombreFuncion);

		textFieldNombreFuncion = new JTextField();
		textFieldNombreFuncion.setFont(new Font("Arial", Font.PLAIN, 15));
		textFieldNombreFuncion.setBounds(135, 150, 193, 20);
		getContentPane().add(textFieldNombreFuncion);
		textFieldNombreFuncion.setColumns(10);

		spinerHora = new JSpinner();
		spinerHora.setModel(new SpinnerNumberModel(00, 00, 24, 1));
		spinerHora.setBounds(135, 300, 49, 22);
		getContentPane().add(spinerHora);

		lblNewLabel = new JLabel(":");
		lblNewLabel.setBounds(185, 300, 38, 21);
		getContentPane().add(lblNewLabel);

		spinerMin = new JSpinner();
		spinerMin.setModel(new SpinnerNumberModel(00, 00, 55, 5));
		spinerMin.setBounds(190, 300, 49, 22);
		getContentPane().add(spinerMin);

		JLabel lblNewLabel_1 = new JLabel("Campos obligatorios (*)");
		lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(25, 450, 159, 16);
		getContentPane().add(lblNewLabel_1);

		lblImagen = new JLabel("Imagen Pelicula");
		lblImagen.setVerticalAlignment(SwingConstants.TOP);
		lblImagen.setBounds(450, 52, 267, 263);
		getContentPane().add(lblImagen);

		lblDescripcion = new JLabel("Descripci\u00F3n");
		lblDescripcion.setForeground(Color.BLACK);
		lblDescripcion.setFont(new Font("Arial", Font.PLAIN, 15));
		lblDescripcion.setBounds(450, 326, 110, 20);
		getContentPane().add(lblDescripcion);

		textAreaDescripcion = new JTextArea();
		textAreaDescripcion.setForeground(Color.BLACK);
		textAreaDescripcion.setBackground(Color.LIGHT_GRAY);
		textAreaDescripcion.setEditable(false);
		textAreaDescripcion.setFont(new Font("Arial", Font.PLAIN, 15));
		textAreaDescripcion.setBounds(450, 375, 332, 123);
		getContentPane().add(textAreaDescripcion);

	}

	public JTextField getTxtFieldCodigoFuncion() {
		return txtFieldCodigoFuncion;
	}

	public void setTxtFieldCodigoFuncion(JTextField txtFieldCodigoFuncion) {
		this.txtFieldCodigoFuncion = txtFieldCodigoFuncion;
	}

	public JLabel getLblCodigo() {
		return lblCodigo;
	}

	public void setLblCodigo(JLabel lblCodigo) {
		this.lblCodigo = lblCodigo;
	}

	public JLabel getLblPelicula() {
		return lblPelicula;
	}

	public void setLblPelicula(JLabel lblPelicula) {
		this.lblPelicula = lblPelicula;
	}

	public JLabel getLblHorario() {
		return lblHorario;
	}

	public void setLblHorario(JLabel lblHorario) {
		this.lblHorario = lblHorario;
	}

	public JLabel getLblSala() {
		return lblSala;
	}

	public void setLblSala(JLabel lblSala) {
		this.lblSala = lblSala;
	}

	public JTextField getTxtFieldPrecio() {
		return txtFieldPrecio;
	}

	public void setTxtFieldPrecio(JTextField txtFieldPrecio) {
		this.txtFieldPrecio = txtFieldPrecio;
	}

	public JComboBox getCmboBxSala() {
		return cmboBxSala;
	}

	public void setCmboBxSala(JComboBox cmboBxSala) {
		this.cmboBxSala = cmboBxSala;
	}

	public JComboBox getCmboBxPelicula() {
		return cmboBxPelicula;
	}

	public void setCmboBxPelicula(JComboBox cmboBxPelicula) {
		this.cmboBxPelicula = cmboBxPelicula;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JDateChooser getDateChooser() {
		return dateChooser;
	}

	public void setDateChooser(JDateChooser dateChooser) {
		this.dateChooser = dateChooser;
	}

	public JTextField getTextFieldNombreFuncion() {
		return textFieldNombreFuncion;
	}

	public void setTextFieldNombreFuncion(JTextField textFieldNombreFuncion) {
		this.textFieldNombreFuncion = textFieldNombreFuncion;
	}

	public JSpinner getSpinerHora() {
		return spinerHora;
	}

	public void setSpinerHora(JSpinner spinerHora) {
		this.spinerHora = spinerHora;
	}

	public JSpinner getSpinerMin() {
		return spinerMin;
	}

	public void setSpinerMin(JSpinner spinerMin) {
		this.spinerMin = spinerMin;
	}

	public JLabel getLblImagen() {
		return lblImagen;
	}

	public void setLblImagen(JLabel lblImagen) {
		this.lblImagen = lblImagen;
	}

	public JTextArea getTextAreaDescripcion() {
		return textAreaDescripcion;
	}

	public void setTextAreaDescripcion(JTextArea textAreaDescripcion) {
		this.textAreaDescripcion = textAreaDescripcion;
	}

}
