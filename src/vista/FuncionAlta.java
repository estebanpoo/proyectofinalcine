package vista;

import controlador.ControladorAltaFuncion;

@SuppressWarnings("serial")
public class FuncionAlta extends FuncionAM {

	private ControladorAltaFuncion caF;

	public FuncionAlta(ControladorAltaFuncion caF) {
		this.setCaF(caF);
		setTitle("Nueva funci�n");
		this.getBtnGuardar().addActionListener(this.getCaF());
		this.getBtnCancelar().addActionListener(this.getCaF());
		this.getTxtFieldCodigoFuncion().addKeyListener(this.getCaF());
		this.getTxtFieldPrecio().addKeyListener(getCaF());
		this.addWindowListener(this.getCaF());
		this.getCmboBxPelicula().addItemListener(this.getCaF());

	}

	public ControladorAltaFuncion getCaF() {
		return caF;
	}

	public void setCaF(ControladorAltaFuncion caF) {
		this.caF = caF;
	}

}
