package vista;

import controlador.ControladorModificacionFuncion;

@SuppressWarnings("serial")
public class FuncionModificar extends FuncionAM {

	private ControladorModificacionFuncion cmF;

	public FuncionModificar(ControladorModificacionFuncion cmF) {
		this.setCmF(cmF);
		this.setTitle("Modificar funci�n");
		this.getTxtFieldCodigoFuncion().setEditable(false);
		this.getBtnGuardar().addActionListener(this.getCmF());
		this.getBtnCancelar().addActionListener(this.getCmF());
		this.getTxtFieldCodigoFuncion().addKeyListener(this.getCmF());
		this.addWindowListener(this.getCmF());
		this.getCmboBxPelicula().addItemListener(this.getCmF());

	}

	public ControladorModificacionFuncion getCmF() {
		return cmF;
	}

	public void setCmF(ControladorModificacionFuncion cmF) {
		this.cmF = cmF;
	}

}
