package vista;

import java.awt.Font;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JTextArea;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public abstract class PeliculaAM extends JDialog {

	private JTextField txtDuracion;
	private JTextField txtNombre;
	private JTextField txtCodigo;
	private JButton btnElegir;
	private JLabel lblImagen;
	private JComboBox comboBoxGenero;
	private JButton btnGuardar;
	private JButton btnCancelar;
	private String direccion;
	private FileInputStream fis;
	private JComboBox comboBoxCondicion;
	private JComboBox comboBoxAudio;
	private JSpinner spinnerPuntaje;
	private JTextArea areaDescripcion;
	private JLabel lblNewLabel_1;

	public PeliculaAM() {
		this.setIconImage(
				Toolkit.getDefaultToolkit().getImage(PeliculaAM.class.getResource("/imagenesPeliculas/peliculas.jpg")));
		this.getContentPane().setBackground(Color.LIGHT_GRAY);
		this.setResizable(false);
		this.setModal(true);
		this.setBounds(100, 100, 800, 600);
		this.getContentPane().setLayout(null);

		JLabel lblCodigo = new JLabel("C\u00F3digo*");
		lblCodigo.setForeground(Color.BLACK);
		lblCodigo.setFont(new Font("Arial", Font.PLAIN, 15));
		lblCodigo.setBounds(25, 50, 100, 20);
		getContentPane().add(lblCodigo);

		JLabel labelNombre = new JLabel("Nombre*");
		labelNombre.setForeground(Color.BLACK);
		labelNombre.setFont(new Font("Arial", Font.PLAIN, 15));
		labelNombre.setBounds(25, 100, 100, 20);
		getContentPane().add(labelNombre);

		JLabel lblAudio = new JLabel("Audio");
		lblAudio.setForeground(Color.BLACK);
		lblAudio.setFont(new Font("Arial", Font.PLAIN, 15));
		lblAudio.setBounds(25, 150, 100, 20);
		getContentPane().add(lblAudio);

		JLabel lblGenero = new JLabel("Genero");
		lblGenero.setForeground(Color.BLACK);
		lblGenero.setFont(new Font("Arial", Font.PLAIN, 15));
		lblGenero.setBounds(25, 200, 100, 20);
		getContentPane().add(lblGenero);

		JLabel lblPuntaje = new JLabel("Puntaje (0..5)*");
		lblPuntaje.setForeground(Color.BLACK);
		lblPuntaje.setFont(new Font("Arial", Font.PLAIN, 15));
		lblPuntaje.setBounds(25, 250, 103, 20);
		getContentPane().add(lblPuntaje);

		JLabel lblDuracion = new JLabel("Duraci\u00F3n (min)*");
		lblDuracion.setForeground(Color.BLACK);
		lblDuracion.setFont(new Font("Arial", Font.PLAIN, 15));
		lblDuracion.setBounds(25, 300, 103, 20);
		getContentPane().add(lblDuracion);

		txtDuracion = new JTextField();
		txtDuracion.setFont(new Font("Arial", Font.PLAIN, 15));
		txtDuracion.setBounds(135, 299, 55, 22);
		getContentPane().add(txtDuracion);
		txtDuracion.setColumns(10);

		comboBoxGenero = new JComboBox();
		comboBoxGenero.setMaximumRowCount(5);
		comboBoxGenero.setFont(new Font("Arial", Font.PLAIN, 15));
		comboBoxGenero.setModel(new DefaultComboBoxModel(new String[] { "No especifica", "Accion ", "Aventura",
				"Belica", "Ciencia Ficcion", "Comedia", "Drama", "Infantil", "Musical", "Suspenso", "Terror" }));
		comboBoxGenero.setBounds(135, 200, 175, 22);
		getContentPane().add(comboBoxGenero);

		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Arial", Font.PLAIN, 15));
		txtNombre.setBounds(135, 100, 175, 22);
		getContentPane().add(txtNombre);
		txtNombre.setColumns(10);

		txtCodigo = new JTextField();
		txtCodigo.setFont(new Font("Arial", Font.PLAIN, 15));
		txtCodigo.setBounds(135, 50, 55, 22);
		getContentPane().add(txtCodigo);
		txtCodigo.setColumns(10);

		lblImagen = new JLabel("");
		lblImagen.setBounds(450, 52, 250, 263);
		getContentPane().add(lblImagen);

		btnElegir = new JButton("Elegir");
		btnElegir.setForeground(Color.BLACK);
		btnElegir.setBackground(SystemColor.activeCaption);
		btnElegir.setFont(new Font("Arial", Font.PLAIN, 15));
		btnElegir.setBounds(529, 348, 97, 25);
		getContentPane().add(btnElegir);

		btnGuardar = new JButton("Guardar");
		btnGuardar.setBackground(SystemColor.activeCaption);
		btnGuardar.setForeground(Color.BLACK);
		btnGuardar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnGuardar.setBounds(300, 450, 97, 25);
		getContentPane().add(btnGuardar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBackground(SystemColor.activeCaption);
		btnCancelar.setForeground(Color.BLACK);
		btnCancelar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnCancelar.setBounds(300, 500, 97, 25);
		getContentPane().add(btnCancelar);

		JLabel lblCondicion = new JLabel("Condici\u00F3n");
		lblCondicion.setForeground(Color.BLACK);
		lblCondicion.setFont(new Font("Arial", Font.PLAIN, 15));
		lblCondicion.setBounds(25, 350, 100, 20);
		getContentPane().add(lblCondicion);

		comboBoxCondicion = new JComboBox();
		comboBoxCondicion.setMaximumRowCount(5);
		comboBoxCondicion
				.setModel(new DefaultComboBoxModel(new String[] { "No especifica", "ATP", "+13", "+16", "+18", "C" }));
		comboBoxCondicion.setFont(new Font("Arial", Font.PLAIN, 15));
		comboBoxCondicion.setBounds(135, 350, 175, 22);
		getContentPane().add(comboBoxCondicion);

		comboBoxAudio = new JComboBox();
		comboBoxAudio.setModel(new DefaultComboBoxModel(new String[] { "No especifica", "Castellano", "Ingles",
				"Castellano e ingles", "Otro", "Casetellano y otro" }));
		comboBoxAudio.setMaximumRowCount(5);
		comboBoxAudio.setFont(new Font("Arial", Font.PLAIN, 15));
		comboBoxAudio.setBounds(135, 150, 175, 22);
		getContentPane().add(comboBoxAudio);

		JLabel lblNewLabel = new JLabel("Imagen pel\u00EDcula*");
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNewLabel.setBounds(450, 23, 115, 16);
		getContentPane().add(lblNewLabel);

		spinnerPuntaje = new JSpinner();
		spinnerPuntaje.setModel(new SpinnerNumberModel(1.0, 0.0, 5.0, 0.1));
		spinnerPuntaje.setFont(new Font("Arial", Font.PLAIN, 15));
		spinnerPuntaje.setBounds(137, 249, 55, 22);
		getContentPane().add(spinnerPuntaje);

		areaDescripcion = new JTextArea();
		areaDescripcion.setFont(new Font("Arial", Font.PLAIN, 15));
		areaDescripcion.setBounds(450, 429, 332, 123);
		getContentPane().add(areaDescripcion);

		JLabel lblDescripcin = new JLabel("Descripci\u00F3n*");
		lblDescripcin.setForeground(Color.BLACK);
		lblDescripcin.setFont(new Font("Arial", Font.PLAIN, 15));
		lblDescripcin.setBounds(450, 396, 100, 20);
		getContentPane().add(lblDescripcin);

		lblNewLabel_1 = new JLabel("Campos obligatorios(*)");
		lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(26, 450, 161, 16);
		getContentPane().add(lblNewLabel_1);
		setLocationRelativeTo(null);
		this.setDireccion(System.getProperty("user.dir\\src\\imagenesPeliculas\\"));
	}

	public void mostrarImagen(String dire) {
		File f = new File(dire);

		Image foto = null;
		try {
			foto = ImageIO.read(f).getScaledInstance(getLblImagen().getWidth(), getLblImagen().getHeight(),
					Image.SCALE_DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}

		getLblImagen().setIcon(new ImageIcon(foto));
		getLblImagen().updateUI();

	}

	public JButton getBtnElegir() {
		return btnElegir;
	}

	public void setBtnElegir(JButton btnElegir) {
		this.btnElegir = btnElegir;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JLabel getLblImagen() {
		return lblImagen;
	}

	public void setLblImagen(JLabel lblImagen) {
		this.lblImagen = lblImagen;
	}

	public JComboBox getComboBoxAudio() {
		return comboBoxAudio;
	}

	public void setComboBoxAudio(JComboBox comboBoxAudio) {
		this.comboBoxAudio = comboBoxAudio;
	}

	public JComboBox getComboBoxGenero() {
		return comboBoxGenero;
	}

	public void setComboBoxGenero(JComboBox comboBoxGenero) {
		this.comboBoxGenero = comboBoxGenero;
	}

	public JTextField getTxtDuracion() {
		return txtDuracion;
	}

	public void setTxtDuracion(JTextField txtDuracion) {
		this.txtDuracion = txtDuracion;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JTextField getTxtCodigo() {
		return txtCodigo;
	}

	public void setTxtCodigo(JTextField txtCodigo) {
		this.txtCodigo = txtCodigo;
	}

	public JComboBox getComboBoxCondicion() {
		return comboBoxCondicion;
	}

	public void setComboBoxCondicion(JComboBox comboBoxCondicion) {
		this.comboBoxCondicion = comboBoxCondicion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public FileInputStream getFis() {
		return fis;
	}

	public void setFis(FileInputStream fis) {
		this.fis = fis;
	}

	public JSpinner getSpinnerPuntaje() {
		return spinnerPuntaje;
	}

	public void setSpinnerPuntaje(JSpinner spinnerPuntaje) {
		this.spinnerPuntaje = spinnerPuntaje;
	}

	public JTextArea getAreaDescripcion() {
		return areaDescripcion;
	}

	public void setAreaDescripcion(JTextArea areaDescripcion) {
		this.areaDescripcion = areaDescripcion;
	}

}
