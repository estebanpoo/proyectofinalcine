package vista;

import controlador.ControladorAltaPelicula;

@SuppressWarnings("serial")
public class PeliculaAlta extends PeliculaAM {

	private ControladorAltaPelicula cap;

	public PeliculaAlta(ControladorAltaPelicula cap) {
		this.setTitle("Alta de peliculas");
		this.setCap(cap);
		this.getTxtCodigo().setEditable(false);
		this.getBtnGuardar().addActionListener(this.getCap());
		this.getBtnCancelar().addActionListener(this.getCap());
		this.getBtnElegir().addActionListener(this.getCap());
		this.getTxtCodigo().addKeyListener(this.getCap());
		this.getTxtDuracion().addKeyListener(this.getCap());

	}

	public ControladorAltaPelicula getCap() {
		return cap;
	}

	public void setCap(ControladorAltaPelicula cap) {
		this.cap = cap;
	}
}
