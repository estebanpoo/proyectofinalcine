package vista;

import controlador.ControladorModificacionPelicula;

@SuppressWarnings("serial")
public class PeliculaModificacion extends PeliculaAM {

	private ControladorModificacionPelicula cmp;

	public PeliculaModificacion(ControladorModificacionPelicula cmp) {
		this.setCmp(cmp);
		setTitle("Modificación de peliculas");
		this.addWindowListener(this.getCmp());
		this.getBtnGuardar().addActionListener(this.getCmp());
		this.getBtnCancelar().addActionListener(this.getCmp());
		this.getBtnElegir().addActionListener(this.getCmp());
		this.getTxtCodigo().setEditable(false);
	}

	public ControladorModificacionPelicula getCmp() {
		return cmp;
	}

	public void setCmp(ControladorModificacionPelicula cmp) {
		this.cmp = cmp;
	}

}
