package vista;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public abstract class SalaAM extends JDialog {
	private JTextField textFieldCodigo;
	private JTextField textFieldNombre;
	private JTextField textFieldCapVertical;
	private JTextField textFieldCapHorizontal;
	private JButton btnGuardar;
	private JButton btnCancelar;
	private JLabel lblNewLabel;

	public SalaAM() {
		this.setIconImage(
				Toolkit.getDefaultToolkit().getImage(SalaAM.class.getResource("/imagenesPeliculas/salas.jpg")));
		this.getContentPane().setBackground(Color.LIGHT_GRAY);
		this.setTitle("Sala");
		this.setModal(true);
		this.setBounds(100, 100, 800, 600);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(0, 0, 784, 560);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblCodigo = new JLabel("C\u00F3digo*");
		lblCodigo.setForeground(Color.BLACK);
		lblCodigo.setFont(new Font("Arial", Font.PLAIN, 15));
		lblCodigo.setBounds(180, 50, 72, 28);
		panel.add(lblCodigo);

		textFieldCodigo = new JTextField();
		textFieldCodigo.setFont(new Font("Arial", Font.PLAIN, 15));
		textFieldCodigo.setBounds(264, 50, 124, 20);
		textFieldCodigo.setColumns(10);
		panel.add(textFieldCodigo);

		JLabel lblNombre = new JLabel("Nombre*");
		lblNombre.setForeground(Color.BLACK);
		lblNombre.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNombre.setBounds(180, 100, 72, 20);
		panel.add(lblNombre);

		textFieldNombre = new JTextField();
		textFieldNombre.setFont(new Font("Arial", Font.PLAIN, 15));
		textFieldNombre.setBounds(264, 100, 124, 20);
		textFieldNombre.setColumns(10);
		panel.add(textFieldNombre);

		JLabel lblAsientos = new JLabel("Asientos*");
		lblAsientos.setForeground(Color.BLACK);
		lblAsientos.setFont(new Font("Arial", Font.PLAIN, 15));
		lblAsientos.setBounds(180, 150, 72, 28);
		panel.add(lblAsientos);

		textFieldCapVertical = new JTextField();
		textFieldCapVertical.setFont(new Font("Arial", Font.PLAIN, 15));
		textFieldCapVertical.setBounds(264, 150, 124, 20);
		textFieldCapVertical.setColumns(10);
		panel.add(textFieldCapVertical);

		JLabel lblCapacidadVertical = new JLabel("Capacidad Vertical*");
		lblCapacidadVertical.setFont(new Font("Arial", Font.PLAIN, 15));
		lblCapacidadVertical.setForeground(Color.BLACK);
		lblCapacidadVertical.setBounds(398, 158, 140, 14);
		panel.add(lblCapacidadVertical);

		textFieldCapHorizontal = new JTextField();
		textFieldCapHorizontal.setFont(new Font("Arial", Font.PLAIN, 15));
		textFieldCapHorizontal.setBounds(264, 200, 124, 20);
		textFieldCapHorizontal.setColumns(10);
		panel.add(textFieldCapHorizontal);

		JLabel lblCapacidadHorizontal = new JLabel("Capacidad Horizontal*");
		lblCapacidadHorizontal.setFont(new Font("Arial", Font.PLAIN, 15));
		lblCapacidadHorizontal.setForeground(Color.BLACK);
		lblCapacidadHorizontal.setBounds(398, 204, 159, 14);
		panel.add(lblCapacidadHorizontal);

		btnGuardar = new JButton("Guardar");
		btnGuardar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnGuardar.setForeground(Color.BLACK);
		btnGuardar.setBackground(SystemColor.activeCaption);
		btnGuardar.setBounds(339, 380, 110, 23);
		panel.add(btnGuardar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnCancelar.setBackground(SystemColor.activeCaption);
		btnCancelar.setForeground(Color.BLACK);
		btnCancelar.setBounds(461, 380, 110, 23);
		panel.add(btnCancelar);

		lblNewLabel = new JLabel("Campos obligatorios(*)");
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNewLabel.setBounds(180, 300, 159, 16);
		panel.add(lblNewLabel);

	}

	public JTextField getTextFieldCodigo() {
		return textFieldCodigo;
	}

	public void setTextFieldCodigo(JTextField textFieldCodigo) {
		this.textFieldCodigo = textFieldCodigo;
	}

	public JTextField getTextFieldNombre() {
		return textFieldNombre;
	}

	public void setTextFieldNombre(JTextField textFieldNombre) {
		this.textFieldNombre = textFieldNombre;
	}

	public JTextField getTextFieldCapVertical() {
		return textFieldCapVertical;
	}

	public void setTextFieldCapVertical(JTextField textFieldCapVertical) {
		this.textFieldCapVertical = textFieldCapVertical;
	}

	public JTextField getTextFieldCapHorizontal() {
		return textFieldCapHorizontal;
	}

	public void setTextFieldCapHorizontal(JTextField textFieldCapHorizontal) {
		this.textFieldCapHorizontal = textFieldCapHorizontal;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

}
