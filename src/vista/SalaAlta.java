package vista;

import controlador.ControladorAltaSala;

@SuppressWarnings("serial")
public class SalaAlta extends SalaAM {

	private ControladorAltaSala cas;

	public SalaAlta(ControladorAltaSala cas) {

		this.setCas(cas);
		setTitle("Sala - Nueva Sala");
		this.getBtnGuardar().addActionListener(this.getCas());
		this.getBtnCancelar().addActionListener(this.getCas());
		this.getTextFieldCodigo().addKeyListener(this.getCas());
		this.getTextFieldCapHorizontal().addKeyListener(this.getCas());
		this.getTextFieldCapVertical().addKeyListener(this.getCas());
	}

	public ControladorAltaSala getCas() {
		return cas;
	}

	public void setCas(ControladorAltaSala cas) {
		this.cas = cas;
	}

}
