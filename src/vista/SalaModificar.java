package vista;

import controlador.ControladorModificacionSala;

@SuppressWarnings("serial")
public class SalaModificar extends SalaAM {

	private ControladorModificacionSala cms;

	public SalaModificar(ControladorModificacionSala cms) {
		this.setCms(cms);
		this.setTitle("Sala - Modificar Sala");
		this.getTextFieldCodigo().setEditable(false);
		this.getBtnGuardar().addActionListener(this.getCms());
		this.getBtnCancelar().addActionListener(this.getCms());
		this.getTextFieldCodigo().addKeyListener(this.getCms());
	}

	public ControladorModificacionSala getCms() {
		return cms;
	}

	public void setCms(ControladorModificacionSala cms) {
		this.cms = cms;
	}

}
