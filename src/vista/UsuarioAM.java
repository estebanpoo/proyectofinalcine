package vista;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

@SuppressWarnings("serial")
public abstract class UsuarioAM extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtDni;
	private JTextField txtUser;
	private JTextField txtApellido;
	private JTextField txtNombre;
	private JButton btnGuardar;
	private JButton btnCancelar;
	private JLabel lblNewLabel_1;
	private JComboBox comboBoxTipo;

	public UsuarioAM() {
		this.getContentPane().setForeground(Color.BLACK);
		this.getContentPane().setBackground(Color.LIGHT_GRAY);
		this.setResizable(false);
		this.setModal(true);

		this.setBounds(100, 100, 800, 600);
		this.getContentPane().setLayout(null);

		txtDni = new JTextField();
		txtDni.setFont(new Font("Arial", Font.PLAIN, 15));
		txtDni.setBounds(438, 100, 116, 22);
		getContentPane().add(txtDni);
		txtDni.setColumns(10);

		JLabel lblDni = new JLabel("DNI*");
		lblDni.setForeground(Color.BLACK);
		lblDni.setFont(new Font("Arial", Font.PLAIN, 15));
		lblDni.setBounds(241, 103, 72, 16);
		getContentPane().add(lblDni);

		txtUser = new JTextField();
		txtUser.setFont(new Font("Arial", Font.PLAIN, 15));
		txtUser.setBounds(438, 247, 116, 22);
		txtUser.setColumns(10);
		getContentPane().add(txtUser);

		txtApellido = new JTextField();
		txtApellido.setFont(new Font("Arial", Font.PLAIN, 15));
		txtApellido.setBounds(438, 197, 116, 22);
		txtApellido.setColumns(10);
		getContentPane().add(txtApellido);

		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Arial", Font.PLAIN, 15));
		txtNombre.setBounds(438, 147, 116, 22);
		txtNombre.setColumns(10);
		getContentPane().add(txtNombre);

		JLabel lblNewLabel = new JLabel("Nombre*");
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNewLabel.setBounds(241, 150, 72, 16);
		getContentPane().add(lblNewLabel);

		JLabel lblApellido = new JLabel("Apellido*");
		lblApellido.setForeground(Color.BLACK);
		lblApellido.setFont(new Font("Arial", Font.PLAIN, 15));
		lblApellido.setBounds(241, 200, 72, 16);
		getContentPane().add(lblApellido);

		JLabel lblNombreDeUsuario = new JLabel("Nombre usuario*");
		lblNombreDeUsuario.setForeground(Color.BLACK);
		lblNombreDeUsuario.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNombreDeUsuario.setBounds(240, 250, 117, 16);
		getContentPane().add(lblNombreDeUsuario);

		btnGuardar = new JButton("Guardar");
		btnGuardar.setBackground(SystemColor.activeCaption);
		btnGuardar.setForeground(Color.BLACK);
		btnGuardar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnGuardar.setBounds(326, 382, 116, 25);
		getContentPane().add(btnGuardar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setForeground(Color.BLACK);
		btnCancelar.setBackground(SystemColor.activeCaption);
		btnCancelar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnCancelar.setBounds(326, 443, 116, 25);
		getContentPane().add(btnCancelar);

		lblNewLabel_1 = new JLabel("Campos obligatorios(*)");
		lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(240, 497, 158, 16);
		getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Tipo*");
		lblNewLabel_2.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNewLabel_2.setBounds(240, 300, 56, 16);
		getContentPane().add(lblNewLabel_2);

		comboBoxTipo = new JComboBox();
		comboBoxTipo.setModel(new DefaultComboBoxModel(new String[] { "Vendedor", "Administrador" }));
		comboBoxTipo.setFont(new Font("Arial", Font.PLAIN, 15));
		comboBoxTipo.setBounds(438, 297, 116, 22);
		getContentPane().add(comboBoxTipo);
		setLocationRelativeTo(null);

	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JTextField getTxtDni() {
		return txtDni;
	}

	public void setTxtDni(JTextField txtDni) {
		this.txtDni = txtDni;
	}

	public JTextField getTxtUser() {
		return txtUser;
	}

	public void setTxtUser(JTextField txtUser) {
		this.txtUser = txtUser;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public void setTxtApellido(JTextField txtApellido) {
		this.txtApellido = txtApellido;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JComboBox getComboBoxTipo() {
		return comboBoxTipo;
	}

	public void setComboBoxTipo(JComboBox comboBoxTipo) {
		this.comboBoxTipo = comboBoxTipo;
	}

}
