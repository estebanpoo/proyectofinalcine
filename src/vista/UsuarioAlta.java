package vista;

import controlador.ControladorAltaUsuario;

@SuppressWarnings("serial")
public class UsuarioAlta extends UsuarioAM {

	private ControladorAltaUsuario cau;

	public UsuarioAlta(ControladorAltaUsuario cau) {
		this.setCau(cau);
		this.setTitle("Alta de usuario");
		this.getTxtDni().addKeyListener(this.getCau());
		this.getBtnGuardar().addActionListener(this.getCau());
		this.getBtnCancelar().addActionListener(this.getCau());
	}

	public ControladorAltaUsuario getCau() {
		return cau;
	}

	public void setCau(ControladorAltaUsuario cau) {
		this.cau = cau;
	}

}
