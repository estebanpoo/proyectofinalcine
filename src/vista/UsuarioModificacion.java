package vista;

import controlador.ControladorModificacionUsuario;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class UsuarioModificacion extends UsuarioAM {
	private ControladorModificacionUsuario cmu;
	private JTextField txtID;

	public UsuarioModificacion(ControladorModificacionUsuario cmu) {
		this.setCmu(cmu);
		this.setTitle("Modificación de usuario");

		JLabel lblNewLabel = new JLabel("Id_usuario");
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNewLabel.setBounds(25, 50, 116, 16);
		getContentPane().add(lblNewLabel);

		txtID = new JTextField();
		txtID.setFont(new Font("Arial", Font.PLAIN, 15));
		txtID.setEditable(false);
		txtID.setBounds(140, 50, 116, 22);
		txtID.setColumns(10);
		getContentPane().add(txtID);

		this.getBtnGuardar().addActionListener(this.getCmu());
		this.getBtnCancelar().addActionListener(this.getCmu());
	}

	public JTextField getTxtID() {
		return txtID;
	}

	public void setTxtID(JTextField txtID) {
		this.txtID = txtID;
	}

	public ControladorModificacionUsuario getCmu() {
		return cmu;
	}

	public void setCmu(ControladorModificacionUsuario cmu) {
		this.cmu = cmu;
	}
}
