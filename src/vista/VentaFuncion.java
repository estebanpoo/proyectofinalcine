package vista;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JSpinner;
import controlador.ControladorVentaFuncion;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.SpinnerNumberModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class VentaFuncion extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JButton btnSiguiente;
	private ControladorVentaFuncion cVentaF;
	private JScrollPane scrollPane;
	private JList<String> list;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JSpinner spinnerEntradas_1;

	public VentaFuncion(ControladorVentaFuncion cVentaF) {
		this.setBackground(SystemColor.controlHighlight);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(VentaFuncion.class.getResource("/imagenesPeliculas/pochoclos.jpg")));
		this.setcVentaF(cVentaF);
		setTitle("Venta - Selecci�n de funci�n");
		this.addWindowListener(this.getcVentaF());
		this.setBounds(100, 100, 800, 600);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		getContentPane().setLayout(null);
		setModal(true);
		getContentPane().setBackground(Color.LIGHT_GRAY);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(10, 11, 774, 549);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblEntradas = new JLabel("Entradas");
		lblEntradas.setForeground(Color.BLACK);
		lblEntradas.setFont(new Font("Arial", Font.PLAIN, 18));
		lblEntradas.setBounds(258, 381, 84, 39);
		panel.add(lblEntradas);

		btnSiguiente = new JButton("Siguiente");
		btnSiguiente.setEnabled(false);
		btnSiguiente.setFont(new Font("Arial", Font.PLAIN, 15));
		btnSiguiente.setBackground(SystemColor.activeCaption);
		btnSiguiente.setForeground(Color.BLACK);
		btnSiguiente.setBounds(597, 474, 108, 31);
		btnSiguiente.addActionListener(this.getcVentaF());
		panel.add(btnSiguiente);

		lblNewLabel = new JLabel("");
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNewLabel.setBounds(290, 13, 366, 24);
		panel.add(lblNewLabel);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(121, 86, 535, 194);
		panel.add(scrollPane);

		list = new JList();
		list.setFont(new Font("Arial", Font.PLAIN, 17));
		list.setBackground(Color.BLACK);
		list.setForeground(Color.WHITE);
		list.addMouseListener(this.getcVentaF());

		scrollPane.setViewportView(list);

		lblNewLabel_1 = new JLabel("Seleccionar funci\u00F3n");
		lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNewLabel_1.setForeground(Color.BLACK);
		lblNewLabel_1.setBounds(121, 57, 221, 16);
		panel.add(lblNewLabel_1);

		spinnerEntradas_1 = new JSpinner();
		spinnerEntradas_1.setFont(new Font("Arial", Font.PLAIN, 15));
		spinnerEntradas_1.setModel(new SpinnerNumberModel(1, 1, 10, 1));
		spinnerEntradas_1.setBounds(342, 388, 49, 24);
		panel.add(spinnerEntradas_1);

	}
	

	public JButton getBtnSiguiente() {
		return btnSiguiente;
	}

	public void setBtnSiguiente(JButton btnSiguiente) {
		this.btnSiguiente = btnSiguiente;
	}

	public ControladorVentaFuncion getcVentaF() {
		return cVentaF;
	}

	public void setcVentaF(ControladorVentaFuncion cVentaF) {
		this.cVentaF = cVentaF;
	}

	public JSpinner getSpinnerEntradas_1() {
		return spinnerEntradas_1;
	}

	public void setSpinnerEntradas_1(JSpinner spinnerEntradas_1) {
		this.spinnerEntradas_1 = spinnerEntradas_1;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JList getList() {
		return list;
	}

	public void setList(JList list) {
		this.list = list;
	}

	public JLabel getLblNewLabel() {
		return lblNewLabel;
	}

	public void setLblNewLabel(JLabel lblNewLabel) {
		this.lblNewLabel = lblNewLabel;
	}
}
