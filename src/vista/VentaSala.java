package vista;

import java.awt.Color;
import java.awt.Graphics2D;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import controlador.ControladorVentaSala;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Label;
import java.awt.SystemColor;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class VentaSala extends JDialog {
	private JPanel panel;
	private JToggleButton matrizBotones[][] = new JToggleButton[6][10];
	private JLabel lblPantallaCine;
	private JButton btnSiguientePagar;
	private ControladorVentaSala vSala;

	public VentaSala(ControladorVentaSala vSala) {
		this.setIconImage(
				Toolkit.getDefaultToolkit().getImage(VentaSala.class.getResource("/imagenesPeliculas/pochoclos.jpg")));
		this.setvSala(vSala);
		setTitle("Venta - Selecci�n de butacas");
		this.addWindowListener(this.getvSala());
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setModal(true);
		setBounds(100, 100, 800, 600);
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBounds(10, 42, 774, 478);
		panel.setLayout(new GridLayout(5, 8, 10, 10));
		UIManager.put("ToggleButton.select", new ColorUIResource(Color.YELLOW));
		UIManager.put("ToolTip.background", new ColorUIResource(Color.WHITE));

		BufferedImage img = null;
		try {
			String ruta = System.getProperty("user.dir");
			img = ImageIO.read(new File(ruta + "\\img\\cinema-seat.png"));
			img = (BufferedImage) escalarImage(img, 45, 60);
		} catch (IOException e) {
			e.printStackTrace();
		}

		int contador = 0;
		for (int x = 1; x < matrizBotones.length; x++) {
			for (int y = 0; y < matrizBotones[x].length; y++) {
				contador++;
				JToggleButton btn = new JToggleButton();
				btn.setName("Fila:" + x + " - Butaca:" + contador);
				btn.setIcon(new ImageIcon(img));
				btn.setToolTipText("<html><h3>" + btn.getName() + "</h3>");
				btn.addMouseListener(this.getvSala());
				panel.add(btn);
			}
		}
		panel.updateUI();
		getContentPane().add(panel);

		lblPantallaCine = new JLabel("Pantalla Cine");
		lblPantallaCine.setForeground(Color.BLACK);
		lblPantallaCine.setFont(new Font("Arial", Font.PLAIN, 18));
		lblPantallaCine.setHorizontalAlignment(SwingConstants.CENTER);
		lblPantallaCine.setBounds(102, 11, 631, 25);
		getContentPane().add(lblPantallaCine);

		btnSiguientePagar = new JButton("Siguiente");
		btnSiguientePagar.setEnabled(false);
		btnSiguientePagar.setForeground(Color.BLACK);
		btnSiguientePagar.setBackground(SystemColor.activeCaption);
		btnSiguientePagar.setBounds(695, 531, 89, 23);
		btnSiguientePagar.addActionListener(this.getvSala());
		getContentPane().add(btnSiguientePagar);

		Label label = new Label("");
		label.setBackground(Color.YELLOW);
		label.setBounds(40, 532, 29, 22);
		getContentPane().add(label);

		JLabel lblSeleccionDeButacas = new JLabel("Seleccion de Butacas");
		lblSeleccionDeButacas.setForeground(Color.BLACK);
		lblSeleccionDeButacas.setBounds(75, 535, 123, 14);
		getContentPane().add(lblSeleccionDeButacas);

		Label label_1 = new Label("");
		label_1.setBackground(SystemColor.inactiveCaption);
		label_1.setBounds(215, 532, 29, 22);
		getContentPane().add(label_1);

		JLabel lblButacasDisponibles = new JLabel("Butacas Disponibles");
		lblButacasDisponibles.setForeground(Color.BLACK);
		lblButacasDisponibles.setBounds(250, 535, 123, 14);
		getContentPane().add(lblButacasDisponibles);

		Label label_2 = new Label("");
		label_2.setBackground(Color.RED);
		label_2.setBounds(397, 532, 29, 22);
		getContentPane().add(label_2);

		JLabel lblButacasOcupadas = new JLabel("Butacas Ocupadas");
		lblButacasOcupadas.setForeground(Color.BLACK);
		lblButacasOcupadas.setBounds(432, 535, 112, 14);
		getContentPane().add(lblButacasOcupadas);

		this.getPanel().addMouseListener(getvSala());
	}

	private Image escalarImage(Image srcImg, int w, int h) {
		BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = img.createGraphics();

		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2.drawImage(srcImg, 0, 0, w, h, null);
		g2.dispose();
		return img;
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JToggleButton[][] getMatrizBotones() {
		return matrizBotones;
	}

	public void setMatrizBotones(JToggleButton[][] matrizBotones) {
		this.matrizBotones = matrizBotones;
	}

	public JButton getBtnSiguientePagar() {
		return btnSiguientePagar;
	}

	public void setBtnSiguientePagar(JButton btnSiguientePagar) {
		this.btnSiguientePagar = btnSiguientePagar;
	}

	public ControladorVentaSala getvSala() {
		return vSala;
	}

	public void setvSala(ControladorVentaSala vSala) {
		this.vSala = vSala;
	}
}
