package vista;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class VistaABM extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JPanel panelMenu;
	private VistaPrincipal vPrincipal;
	private JButton btnAgregar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JScrollPane scrollPane;
	private JTable table;
	private JPanel panelBuscar;
	private JTextField txtBuscar;

	public VistaABM() {
		this.setIconImage(
				Toolkit.getDefaultToolkit().getImage(VistaABM.class.getResource("/imagenesPeliculas/abm.jpg")));
		getContentPane().setForeground(Color.WHITE);
		getContentPane().setBackground(SystemColor.menu);
		this.setModal(true);
		this.setBounds(100, 100, 1200, 800);
		this.setLocationRelativeTo(null);

		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(Color.LIGHT_GRAY);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 627, 128);

		contentPanel.add(scrollPane);

		table = new JTable();
		table.setForeground(SystemColor.activeCaptionText);
		table.setBackground(Color.LIGHT_GRAY);
		table.setFont(new Font("Arial", Font.PLAIN, 13));
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {

		}));

		scrollPane.setViewportView(table);

		panelMenu = new JPanel();
		panelMenu.setForeground(SystemColor.activeCaptionText);
		panelMenu.setBackground(Color.LIGHT_GRAY);
		contentPanel.add(panelMenu, BorderLayout.SOUTH);
		panelMenu.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		btnAgregar = new JButton("Agregar");
		btnAgregar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnAgregar.setForeground(Color.BLACK);
		btnAgregar.setBackground(SystemColor.activeCaption);
		panelMenu.add(btnAgregar);

		btnModificar = new JButton("Modificar");
		btnModificar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnModificar.setForeground(Color.BLACK);
		btnModificar.setBackground(SystemColor.activeCaption);
		panelMenu.add(btnModificar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnEliminar.setForeground(Color.BLACK);
		btnEliminar.setBackground(SystemColor.activeCaption);
		panelMenu.add(btnEliminar);

		panelBuscar = new JPanel();
		panelBuscar.setForeground(SystemColor.activeCaptionText);
		panelBuscar.setBackground(Color.LIGHT_GRAY);
		contentPanel.add(panelBuscar, BorderLayout.NORTH);
		panelBuscar.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		txtBuscar = new JTextField();
		txtBuscar.setFont(new Font("Arial", Font.PLAIN, 15));
		panelBuscar.add(txtBuscar);
		txtBuscar.setColumns(10);
		getTable().setFont(new Font("Arial", Font.PLAIN, 15));
		getTable().setRowHeight(30);
	}

	public VistaPrincipal getvPrincipal() {
		return vPrincipal;
	}

	public void setvPrincipal(VistaPrincipal vPrincipal) {
		this.vPrincipal = vPrincipal;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JPanel getPanelMenu() {
		return panelMenu;
	}

	public void setPanelMenu(JPanel panelMenu) {
		this.panelMenu = panelMenu;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JPanel getPanelBuscar() {
		return panelBuscar;
	}

	public void setPanelBuscar(JPanel panelBuscar) {
		this.panelBuscar = panelBuscar;
	}

	public JTextField getTxtBuscar() {
		return txtBuscar;
	}

	public void setTxtBuscar(JTextField txtBuscar) {
		this.txtBuscar = txtBuscar;
	}

}
