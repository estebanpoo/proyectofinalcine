package vista;

import controlador.ControladorFuncion;
import javax.swing.JLabel;
import java.awt.Font;

@SuppressWarnings("serial")
public class VistaFuncion extends VistaABM {

	private ControladorFuncion cFuncion;

	public VistaFuncion(ControladorFuncion controladorFuncion) {
		this.setcFuncion(controladorFuncion);
		this.setTitle("Funciones");
		this.addWindowListener(this.getcFuncion());

		JLabel lblNewLabel = new JLabel("Buscar funciones");
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 15));
		getPanelBuscar().add(lblNewLabel);

		this.getBtnAgregar().addActionListener(this.getcFuncion());
		this.getBtnModificar().addActionListener(this.getcFuncion());
		this.getBtnEliminar().addActionListener(this.getcFuncion());
		this.getTxtBuscar().addKeyListener(this.getcFuncion());

	}

	public ControladorFuncion getcFuncion() {
		return cFuncion;
	}

	public void setcFuncion(ControladorFuncion cFuncion) {
		this.cFuncion = cFuncion;
	}

}
