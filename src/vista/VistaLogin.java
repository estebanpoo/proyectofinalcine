package vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import controlador.ControladorLogin;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.SwingConstants;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class VistaLogin extends JFrame {

	private JPanel contentPane;
	private ControladorLogin controladorLogin;
	private JPasswordField txtContrase�a;
	private JButton btnIngresar;
	private JButton btnSalir;
	private JTextField txtUsuario;
	private JLabel lblLogin;
	private JLabel lblNewLabel;

	public VistaLogin(ControladorLogin controladorLogin) {
		this.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(VistaLogin.class.getResource("/imagenesPeliculas/IconoLogin.gif")));
		this.setUndecorated(true);
		this.setControladorLogin(controladorLogin);
		this.setTitle("Login");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 450, 400);
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Arial", Font.BOLD, 17));
		lblUsuario.setForeground(Color.BLACK);
		lblUsuario.setBounds(50, 150, 70, 16);
		contentPane.add(lblUsuario);

		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setFont(new Font("Arial", Font.BOLD, 17));
		lblContrasea.setForeground(Color.BLACK);
		lblContrasea.setBounds(50, 225, 98, 16);
		contentPane.add(lblContrasea);

		txtContrase�a = new JPasswordField();
		txtContrase�a.setFont(new Font("Arial", Font.PLAIN, 17));
		txtContrase�a.setBounds(160, 225, 112, 22);
		contentPane.add(txtContrase�a);

		btnIngresar = new JButton("Ingresar");
		btnIngresar.setBackground(SystemColor.activeCaption);
		btnIngresar.setFont(new Font("Arial", Font.PLAIN, 17));
		btnIngresar.addActionListener(getControladorLogin());
		btnIngresar.setBounds(160, 297, 112, 25);
		contentPane.add(btnIngresar);

		btnSalir = new JButton("Salir");
		btnSalir.setBackground(SystemColor.activeCaption);
		btnSalir.setFont(new Font("Arial", Font.PLAIN, 17));
		btnSalir.setBounds(160, 364, 112, 25);
		btnSalir.addActionListener(this.getControladorLogin());
		contentPane.add(btnSalir);

		txtUsuario = new JTextField();
		txtUsuario.setFont(new Font("Arial", Font.PLAIN, 17));
		txtUsuario.setBounds(160, 148, 112, 22);
		txtUsuario.setColumns(10);
		contentPane.add(txtUsuario);

		lblLogin = new JLabel("LOGIN");
		lblLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogin.setFont(new Font("Arial", Font.BOLD, 20));
		lblLogin.setBounds(161, 64, 100, 53);
		contentPane.add(lblLogin);

		lblNewLabel = new JLabel("Sistema - CINE");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 22));
		lblNewLabel.setBounds(103, 29, 229, 44);
		contentPane.add(lblNewLabel);

		this.setLocationRelativeTo(null);
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public void setTxtUsuario(JTextField txtUsuario) {
		this.txtUsuario = txtUsuario;
	}

	public ControladorLogin getControladorLogin() {
		return controladorLogin;
	}

	public void setControladorLogin(ControladorLogin controladorLogin) {
		this.controladorLogin = controladorLogin;
	}

	public JPasswordField getTxtContrase�a() {
		return txtContrase�a;
	}

	public void setTxtContrase�a(JPasswordField txtContrase�a) {
		this.txtContrase�a = txtContrase�a;
	}

	public JButton getBtnIngresar() {
		return btnIngresar;
	}

	public void setBtnIngresar(JButton btnIngresar) {
		this.btnIngresar = btnIngresar;
	}
}
