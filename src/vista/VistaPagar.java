package vista;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import controlador.ControladorVentaPagar;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class VistaPagar extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JButton btnConfirmarYGenerar;
	private ControladorVentaPagar cvp;
	private JLabel lblP;
	private JLabel lblF;
	private JLabel lblH;
	private JLabel lblS;
	private JLabel lblB;
	private JLabel lblI;

	public VistaPagar(ControladorVentaPagar cp) {
		this.setIconImage(
				Toolkit.getDefaultToolkit().getImage(VistaPagar.class.getResource("/imagenesPeliculas/pochoclos.jpg")));
		this.setCvp(cp);
		this.setTitle("Venta - Informe de Venta");
		this.setModal(false);
		this.addWindowListener(this.getCvp());
		this.getContentPane().setForeground(Color.LIGHT_GRAY);
		this.setBounds(100, 100, 800, 600);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setForeground(Color.WHITE);
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(0, 0, 794, 571);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblInformeDeVenta = new JLabel("Informe de Venta");
		lblInformeDeVenta.setForeground(Color.BLACK);
		lblInformeDeVenta.setFont(new Font("Arial", Font.PLAIN, 20));
		lblInformeDeVenta.setBounds(289, 25, 152, 68);
		panel.add(lblInformeDeVenta);

		JLabel lblPelicula = new JLabel("Pelicula");
		lblPelicula.setForeground(Color.BLACK);
		lblPelicula.setFont(new Font("Arial", Font.PLAIN, 15));
		lblPelicula.setBounds(150, 100, 61, 29);
		panel.add(lblPelicula);

		JLabel lblSala = new JLabel("Sala");
		lblSala.setForeground(Color.BLACK);
		lblSala.setFont(new Font("Arial", Font.PLAIN, 15));
		lblSala.setBounds(150, 250, 60, 14);
		panel.add(lblSala);

		JLabel lblButaca = new JLabel("Butaca/s");
		lblButaca.setForeground(Color.BLACK);
		lblButaca.setFont(new Font("Arial", Font.PLAIN, 15));
		lblButaca.setBounds(150, 300, 75, 14);
		panel.add(lblButaca);

		JLabel lblImporteTotal = new JLabel("Importe total ($)");
		lblImporteTotal.setForeground(Color.BLACK);
		lblImporteTotal.setFont(new Font("Arial", Font.PLAIN, 15));
		lblImporteTotal.setBounds(150, 350, 119, 15);
		panel.add(lblImporteTotal);

		btnConfirmarYGenerar = new JButton("Confirmar");
		btnConfirmarYGenerar.setBackground(SystemColor.activeCaption);
		btnConfirmarYGenerar.setForeground(Color.BLACK);
		btnConfirmarYGenerar.setFont(new Font("Arial", Font.PLAIN, 15));
		btnConfirmarYGenerar.setBounds(268, 473, 202, 34);
		this.getBtnConfirmarYGenerar().addActionListener(this.getCvp());
		panel.add(btnConfirmarYGenerar);

		JLabel lblHorario = new JLabel("Horario");
		lblHorario.setForeground(Color.BLACK);
		lblHorario.setFont(new Font("Arial", Font.PLAIN, 15));
		lblHorario.setBounds(150, 200, 75, 20);
		panel.add(lblHorario);

		JLabel lblFecha = new JLabel("Fecha");
		lblFecha.setForeground(Color.BLACK);
		lblFecha.setFont(new Font("Arial", Font.PLAIN, 15));
		lblFecha.setBounds(150, 150, 75, 14);
		panel.add(lblFecha);

		lblP = new JLabel("New label");
		lblP.setForeground(Color.BLACK);
		lblP.setFont(new Font("Arial", Font.PLAIN, 15));
		lblP.setBounds(290, 106, 294, 16);
		panel.add(lblP);

		lblF = new JLabel("New label");
		lblF.setForeground(Color.BLACK);
		lblF.setFont(new Font("Arial", Font.PLAIN, 15));
		lblF.setBounds(290, 150, 294, 16);
		panel.add(lblF);

		lblH = new JLabel("New label");
		lblH.setForeground(Color.BLACK);
		lblH.setFont(new Font("Arial", Font.PLAIN, 15));
		lblH.setBounds(290, 200, 294, 16);
		panel.add(lblH);

		lblS = new JLabel("New label");
		lblS.setForeground(Color.BLACK);
		lblS.setFont(new Font("Arial", Font.PLAIN, 15));
		lblS.setBounds(290, 250, 294, 16);
		panel.add(lblS);

		lblB = new JLabel("");
		lblB.setFont(new Font("Arial", Font.PLAIN, 15));
		lblB.setBounds(290, 298, 220, 16);
		panel.add(lblB);

		lblI = new JLabel("New label");
		lblI.setForeground(Color.BLACK);
		lblI.setFont(new Font("Arial", Font.PLAIN, 15));
		lblI.setBounds(290, 350, 294, 16);
		panel.add(lblI);
	}

	public JLabel getLblP() {
		return lblP;
	}

	public void setLblP(JLabel lblP) {
		this.lblP = lblP;
	}

	public JLabel getLblF() {
		return lblF;
	}

	public void setLblF(JLabel lblF) {
		this.lblF = lblF;
	}

	public JLabel getLblH() {
		return lblH;
	}

	public void setLblH(JLabel lblH) {
		this.lblH = lblH;
	}

	public JLabel getLblS() {
		return lblS;
	}

	public void setLblS(JLabel lblS) {
		this.lblS = lblS;
	}

	public JLabel getLblB() {
		return lblB;
	}

	public void setLblB(JLabel lblB) {
		this.lblB = lblB;
	}

	public JLabel getLblI() {
		return lblI;
	}

	public void setLblI(JLabel lblI) {
		this.lblI = lblI;
	}

	public JLabel getLblNewLabel() {
		return lblP;
	}

	public void setLblNewLabel(JLabel lblNewLabel) {
		this.lblP = lblNewLabel;
	}

	public JButton getBtnConfirmarYGenerar() {
		return btnConfirmarYGenerar;
	}

	public void setBtnConfirmarYGenerar(JButton btnConfirmarYGenerar) {
		this.btnConfirmarYGenerar = btnConfirmarYGenerar;
	}

	public ControladorVentaPagar getCvp() {
		return cvp;
	}

	public void setCvp(ControladorVentaPagar cvp) {
		this.cvp = cvp;
	}

}
