package vista;

import controlador.ControladorPelicula;
import javax.swing.JLabel;
import java.awt.Font;

@SuppressWarnings("serial")
public class VistaPelicula extends VistaABM {

	private ControladorPelicula cPelicula;

	public VistaPelicula(ControladorPelicula cPelicula) {
		this.setcPelicula(cPelicula);
		setTitle("Peliculas");
		this.getBtnAgregar().addActionListener(this.getcPelicula());
		this.getBtnModificar().addActionListener(this.getcPelicula());
		this.getBtnEliminar().addActionListener(this.getcPelicula());
		this.addWindowListener(this.getcPelicula());

		JLabel lblNewLabel = new JLabel("Buscar peliculas");
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 15));
		getPanelBuscar().add(lblNewLabel);
		this.getTxtBuscar().addKeyListener(this.getcPelicula());

	}

	public ControladorPelicula getcPelicula() {
		return cPelicula;
	}

	public void setcPelicula(ControladorPelicula cPelicula) {
		this.cPelicula = cPelicula;
	}

}
