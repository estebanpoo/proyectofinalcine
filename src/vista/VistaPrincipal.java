package vista;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import controlador.ControladorPrincipal;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.Color;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.JLabel;
import java.awt.SystemColor;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class VistaPrincipal extends JFrame {

	private JPanel contentPane;
	private ControladorPrincipal controladorPrincipal;
	private JButton btnVenta;
	private JButton btnFuncion;
	private JButton btnPelicula;
	private JButton btnUsuario;
	private JButton btnSala;
	private JLabel lblFondo;
	private JLabel lblLogeado;
	private JButton btnReporte;

	public VistaPrincipal(ControladorPrincipal controladorPrincipal) {
		this.setIconImage(
				Toolkit.getDefaultToolkit().getImage(VistaPrincipal.class.getResource("/imagenesPeliculas/cine1.jpg")));
		this.setControladorPrincipal(controladorPrincipal);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setBounds(100, 100, 1940, 1050);
		this.setExtendedState(MAXIMIZED_BOTH);
		this.setTitle("Sistema de gesti�n de venta de entradas");
		this.setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		this.addWindowListener(this.getControladorPrincipal());

		BufferedImage img = null;
		try {
			String ruta = System.getProperty("user.dir");
			img = ImageIO.read(new File(ruta + "\\img\\4.jpg"));
			img = (BufferedImage) escalarImage(img, 1940, 1050);
		} catch (IOException e) {
			e.printStackTrace();
		}

		lblFondo = new JLabel(new ImageIcon(img));
		lblFondo.setForeground(Color.BLACK);
		lblFondo.setBounds(0, 0, 1940, 1050);
		lblFondo.setVisible(true);

		JPanel panelTitulo = new JPanel();
		panelTitulo.setForeground(Color.WHITE);
		panelTitulo.setBackground(Color.LIGHT_GRAY);
		contentPane.add(panelTitulo, BorderLayout.NORTH);

		JLabel lblBienvenido = new JLabel("BIENVENIDO");
		lblBienvenido.setForeground(Color.BLACK);
		panelTitulo.add(lblBienvenido);
		lblBienvenido.setFont(new Font("Comic Sans MS", Font.PLAIN, 30));

		JPanel panelPie = new JPanel();
		panelPie.setForeground(Color.WHITE);
		panelPie.setBackground(Color.LIGHT_GRAY);
		FlowLayout fl_panelPie = (FlowLayout) panelPie.getLayout();
		fl_panelPie.setAlignment(FlowLayout.LEFT);
		contentPane.add(panelPie, BorderLayout.SOUTH);

		JLabel lblCreadoPor = new JLabel("Creado por: Antieco, Saavedra y Uribe ");
		lblCreadoPor.setBackground(new Color(0, 0, 0));
		lblCreadoPor.setForeground(Color.BLACK);
		lblCreadoPor.setFont(new Font("Comic Sans MS", Font.PLAIN, 20));
		panelPie.add(lblCreadoPor);

		lblLogeado = new JLabel(
				"                                                                                                                                                                                                  Usuario activo: ");
		lblLogeado.setFont(new Font("Comic Sans MS", Font.PLAIN, 20));
		lblLogeado.setForeground(Color.BLACK);
		panelPie.add(lblLogeado);

		Panel panelMenu = new Panel();
		panelMenu.setBounds(100, 100, 150, 100);
		contentPane.add(panelMenu, BorderLayout.WEST);
		panelMenu.setBackground(Color.LIGHT_GRAY);
		panelMenu.setLayout(null);

		btnVenta = new JButton("Ventas");
		btnVenta.setBackground(SystemColor.activeCaption);
		btnVenta.setForeground(Color.BLACK);
		btnVenta.setBounds(5, 50, 135, 55);
		btnVenta.addActionListener(this.getControladorPrincipal());
		btnVenta.setFont(new Font("Arial", Font.PLAIN, 22));
		panelMenu.add(btnVenta);

		btnFuncion = new JButton("Funciones");
		btnFuncion.setBackground(SystemColor.activeCaption);
		btnFuncion.setForeground(Color.BLACK);
		btnFuncion.setBounds(5, 150, 135, 55);
		btnFuncion.addActionListener(this.getControladorPrincipal());
		btnFuncion.setFont(new Font("Arial", Font.PLAIN, 22));
		panelMenu.add(btnFuncion);

		btnPelicula = new JButton("Pel\u00EDculas");
		btnPelicula.setBackground(SystemColor.activeCaption);
		btnPelicula.setForeground(Color.BLACK);
		btnPelicula.setBounds(5, 250, 135, 55);
		btnPelicula.addActionListener(this.getControladorPrincipal());
		btnPelicula.setFont(new Font("Arial", Font.PLAIN, 22));
		panelMenu.add(btnPelicula);

		btnUsuario = new JButton("Usuarios");
		btnUsuario.setBackground(SystemColor.activeCaption);
		btnUsuario.setForeground(Color.BLACK);
		btnUsuario.setBounds(5, 450, 135, 55);
		btnUsuario.addActionListener(this.getControladorPrincipal());
		btnUsuario.setFont(new Font("Arial", Font.PLAIN, 22));
		panelMenu.add(btnUsuario);

		btnSala = new JButton("Salas");
		btnSala.setBackground(SystemColor.activeCaption);
		btnSala.setForeground(Color.BLACK);
		btnSala.setBounds(5, 350, 135, 55);
		btnSala.addActionListener(this.getControladorPrincipal());
		btnSala.setFont(new Font("Arial", Font.PLAIN, 22));
		panelMenu.add(btnSala);

		btnReporte = new JButton("Reportes");
		btnReporte.setForeground(Color.BLACK);
		btnReporte.setBackground(SystemColor.activeCaption);
		btnReporte.setFont(new Font("Arial", Font.PLAIN, 22));
		btnReporte.setBounds(5, 550, 135, 55);
		btnReporte.addActionListener(getControladorPrincipal());
		panelMenu.add(btnReporte);

		contentPane.add(lblFondo);

	}

	private Image escalarImage(Image srcImg, int w, int h) {
		BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = img.createGraphics();

		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2.drawImage(srcImg, 0, 0, w, h, null);
		g2.dispose();
		return img;
	}

	public ControladorPrincipal getControladorPrincipal() {
		return controladorPrincipal;
	}

	public void setControladorPrincipal(ControladorPrincipal controladorPrincipal) {
		this.controladorPrincipal = controladorPrincipal;
	}

	public JButton getBtnVenta() {
		return btnVenta;
	}

	public void setBtnVenta(JButton btnVenta) {
		this.btnVenta = btnVenta;
	}

	public JButton getBtnFuncion() {
		return btnFuncion;
	}

	public void setBtnFuncion(JButton btnFuncion) {
		this.btnFuncion = btnFuncion;
	}

	public JButton getBtnPelicula() {
		return btnPelicula;
	}

	public void setBtnPelicula(JButton btnPelicula) {
		this.btnPelicula = btnPelicula;
	}

	public JButton getBtnUsuario() {
		return btnUsuario;
	}

	public void setBtnUsuario(JButton btnUsuario) {
		this.btnUsuario = btnUsuario;
	}

	public JButton getBtnSala() {
		return btnSala;
	}

	public void setBtnSala(JButton btnSala) {
		this.btnSala = btnSala;
	}

	public JLabel getLblLogeado() {
		return lblLogeado;
	}

	public void setLblLogeado(JLabel lblLogeado) {
		this.lblLogeado = lblLogeado;
	}

	public JButton getBtnReporte() {
		return btnReporte;
	}

	public void setBtnReporte(JButton btnReporte) {
		this.btnReporte = btnReporte;
	}

}