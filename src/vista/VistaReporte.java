package vista;

import javax.swing.JDialog;
import javax.swing.JButton;
import controlador.ControladorReporte;
import java.awt.Font;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class VistaReporte extends JDialog {
	private JButton btnReporteVentas;
	private ControladorReporte controladorReporte;
	private JButton btnVentasVendedor;

	public VistaReporte(ControladorReporte controlador) {
		this.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(VistaReporte.class.getResource("/imagenesPeliculas/reportes.jpg")));
		this.setControladorReporte(controlador);
		this.setModal(false);
		this.setBounds(100, 100, 800, 600);
		this.getContentPane().setLayout(null);
		this.setLocationRelativeTo(null);
		this.getContentPane().setBackground(Color.LIGHT_GRAY);
		this.setTitle("Reportes");
		btnReporteVentas = new JButton("Reporte ventas por pel\u00EDcula");
		btnReporteVentas.setBackground(SystemColor.activeCaption);
		btnReporteVentas.setForeground(Color.BLACK);
		btnReporteVentas.setFont(new Font("Arial", Font.PLAIN, 25));
		btnReporteVentas.setBounds(150, 100, 500, 100);
		btnReporteVentas.addActionListener(this.getControladorReporte());
		getContentPane().add(btnReporteVentas);

		btnVentasVendedor = new JButton("Reporte ventas por vendedor");
		btnVentasVendedor.setBackground(SystemColor.activeCaption);
		btnVentasVendedor.setForeground(Color.BLACK);
		btnVentasVendedor.setFont(new Font("Arial", Font.PLAIN, 25));
		btnVentasVendedor.setBounds(150, 300, 500, 100);
		btnVentasVendedor.addActionListener(this.getControladorReporte());
		getContentPane().add(btnVentasVendedor);

	}

	public JButton getBtnReporteVentas() {
		return btnReporteVentas;
	}

	public void setBtnReporteVentas(JButton btnReporteVentas) {
		this.btnReporteVentas = btnReporteVentas;
	}

	public ControladorReporte getControladorReporte() {
		return controladorReporte;
	}

	public void setControladorReporte(ControladorReporte controladorReporte) {
		this.controladorReporte = controladorReporte;
	}

	public JButton getBtnVentasVendedor() {
		return btnVentasVendedor;
	}

	public void setBtnVentasVendedor(JButton btnVentasVendedor) {
		this.btnVentasVendedor = btnVentasVendedor;
	}

}
