package vista;

import javax.swing.JDialog;
import controlador.ControladorReporteVentasVendedor;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class VistaReporteVendedor extends JDialog {

	private ControladorReporteVentasVendedor crvv;
	private JDateChooser dateChooserDesde;
	private JDateChooser dateChooserHasta;
	private JButton btnGenerarReporte;
	private JComboBox comboBox;

	public VistaReporteVendedor(ControladorReporteVentasVendedor crvv) {
		this.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(VistaReporteVendedor.class.getResource("/imagenesPeliculas/reportes.jpg")));
		this.setCrvv(crvv);
		this.addWindowListener(this.getCrvv());
		this.setModal(true);
		this.setTitle("Reporte de ventas por vendedor");
		this.setBounds(100, 100, 800, 600);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		getContentPane().setBackground(Color.LIGHT_GRAY);

		JLabel lblVendedor = new JLabel("Vendedor");
		lblVendedor.setFont(new Font("Arial", Font.PLAIN, 15));
		lblVendedor.setBounds(205, 120, 96, 26);
		getContentPane().add(lblVendedor);

		comboBox = new JComboBox();
		comboBox.setMaximumRowCount(5);
		comboBox.setFont(new Font("Arial", Font.PLAIN, 15));
		comboBox.setBounds(394, 123, 150, 20);
		getContentPane().add(comboBox);

		JLabel lblFechaDesde = new JLabel("Fecha desde:");
		lblFechaDesde.setFont(new Font("Arial", Font.PLAIN, 15));
		lblFechaDesde.setBounds(206, 208, 95, 26);
		getContentPane().add(lblFechaDesde);

		dateChooserDesde = new JDateChooser();
		dateChooserDesde.setDateFormatString("dd/MM/yyyy");
		dateChooserDesde.setBounds(394, 208, 150, 20);
		getContentPane().add(dateChooserDesde);
		Date date = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String fechaSistema = formato.format(date);
		try {
			java.util.Date dato = formato.parse(fechaSistema);
			dateChooserDesde.setMaxSelectableDate(dato);
			String fecha = "";
			dato = formato.parse(fecha);
			dateChooserDesde.setDate(dato);
		} catch (Exception e) {

		}

		JLabel lblFechaHasta = new JLabel("Fecha hasta:");
		lblFechaHasta.setFont(new Font("Arial", Font.PLAIN, 15));
		lblFechaHasta.setBounds(205, 296, 96, 26);
		getContentPane().add(lblFechaHasta);

		dateChooserHasta = new JDateChooser();
		dateChooserHasta.setDateFormatString("dd/MM/yyyy");
		dateChooserHasta.setBounds(394, 296, 150, 20);
		getContentPane().add(dateChooserHasta);
		Date date2 = new Date();
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
		String fechaSistema2 = formato2.format(date2);
		try {
			java.util.Date dato2 = formato2.parse(fechaSistema2);
			dateChooserHasta.setMaxSelectableDate(dato2);
			String fecha2 = "";
			dato2 = formato2.parse(fecha2);
			dateChooserHasta.setDate(dato2);
		} catch (Exception e) {

		}

		btnGenerarReporte = new JButton("Generar Reporte");
		btnGenerarReporte.setForeground(Color.BLACK);
		btnGenerarReporte.setBackground(SystemColor.activeCaption);
		btnGenerarReporte.setFont(new Font("Arial", Font.PLAIN, 15));
		btnGenerarReporte.setBounds(307, 403, 150, 23);
		btnGenerarReporte.addActionListener(this.getCrvv());
		getContentPane().add(btnGenerarReporte);

	}

	public ControladorReporteVentasVendedor getCrvv() {
		return crvv;
	}

	public void setCrvv(ControladorReporteVentasVendedor crvv) {
		this.crvv = crvv;
	}

	public JDateChooser getDateChooserDesde() {
		return dateChooserDesde;
	}

	public void setDateChooserDesde(JDateChooser dateChooserDesde) {
		this.dateChooserDesde = dateChooserDesde;
	}

	public JDateChooser getDateChooserHasta() {
		return dateChooserHasta;
	}

	public void setDateChooserHasta(JDateChooser dateChooserHasta) {
		this.dateChooserHasta = dateChooserHasta;
	}

	public JButton getBtnGenerarReporte() {
		return btnGenerarReporte;
	}

	public void setBtnGenerarReporte(JButton btnGenerarReporte) {
		this.btnGenerarReporte = btnGenerarReporte;
	}

	public JComboBox getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}

}
