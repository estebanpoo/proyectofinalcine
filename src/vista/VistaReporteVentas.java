package vista;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import com.toedter.calendar.JDayChooser;

import controlador.ControladorReporte;
import controlador.ControladorReporteVenta;
import net.sf.jasperreports.engine.util.Java14BigDecimalHandler;

import com.toedter.calendar.JDateChooser;
import javax.swing.JComboBox;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class VistaReporteVentas extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JButton btnGenerarReporte;
	private ControladorReporteVenta crv;
	private JComboBox comboBox;
	private JDateChooser dcDesde;
	private JDateChooser dcHasta;

	public VistaReporteVentas(ControladorReporteVenta crv) {
		this.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(VistaReporteVentas.class.getResource("/imagenesPeliculas/reportes.jpg")));
		this.setSize(400, 400);
		this.setCrv(crv);
		this.setModal(true);
		this.setTitle("Reportes de ventas por pel�cula");
		this.setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(Color.LIGHT_GRAY);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		contentPanel.setLayout(null);
		getContentPane().setBackground(Color.LIGHT_GRAY);
		addWindowListener(this.getCrv());
		{
			JLabel lblNewLabel = new JLabel("Seleccionar fechas");
			lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 15));
			lblNewLabel.setBounds(249, 167, 172, 30);
			contentPanel.add(lblNewLabel);
		}
		{
			JLabel lblNewLabel_1 = new JLabel("Fecha Desde:");
			lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 15));
			lblNewLabel_1.setBounds(140, 212, 172, 30);
			contentPanel.add(lblNewLabel_1);
		}

		dcDesde = new JDateChooser();
		dcDesde.setDateFormatString("dd/MM/yyyy");
		dcDesde.setBounds(378, 222, 172, 20);
		contentPanel.add(dcDesde);
		Date date = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String fechaSistema = formato.format(date);
		try {
			java.util.Date dato = formato.parse(fechaSistema);
			dcDesde.setMaxSelectableDate(dato);
			String fecha = "";
			dato = formato.parse(fecha);
			dcDesde.setDate(dato);
		} catch (Exception e) {

		}

		JLabel lblFechaHasta = new JLabel("Fecha Hasta:");
		lblFechaHasta.setFont(new Font("Arial", Font.PLAIN, 15));
		lblFechaHasta.setBounds(140, 299, 172, 20);
		contentPanel.add(lblFechaHasta);
		dcHasta = new JDateChooser();
		dcHasta.setDateFormatString("dd/MM/yyyy");
		dcHasta.setBounds(378, 299, 172, 20);
		contentPanel.add(dcHasta);
		Date date2 = new Date();
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
		String fechaSistema2 = formato.format(date2);
		try {
			java.util.Date dato2 = formato2.parse(fechaSistema2);
			dcHasta.setMaxSelectableDate(dato2);
			String fecha2 = "";
			dato2 = formato.parse(fecha2);
			dcHasta.setDate(dato2);
		} catch (Exception e) {

		}

		JLabel lblSeleccionarPelicula = new JLabel("Seleccionar pelicula");
		lblSeleccionarPelicula.setFont(new Font("Arial", Font.PLAIN, 15));
		lblSeleccionarPelicula.setBounds(140, 120, 172, 36);
		contentPanel.add(lblSeleccionarPelicula);

		comboBox = new JComboBox();
		comboBox.setMaximumRowCount(5);
		comboBox.setFont(new Font("Arial", Font.PLAIN, 15));
		comboBox.setBounds(378, 128, 172, 20);
		contentPanel.add(comboBox);

		btnGenerarReporte = new JButton("Generar Reporte");
		btnGenerarReporte.setBackground(SystemColor.activeCaption);
		btnGenerarReporte.setForeground(Color.BLACK);
		btnGenerarReporte.setFont(new Font("Arial", Font.PLAIN, 15));
		btnGenerarReporte.setBounds(249, 399, 172, 23);
		btnGenerarReporte.addActionListener(this.getCrv());
		contentPanel.add(btnGenerarReporte);
	}

	public ControladorReporteVenta getCrv() {
		return crv;
	}

	public void setCrv(ControladorReporteVenta crv) {
		this.crv = crv;
	}

	public JButton getBtnGenerarReporte() {
		return btnGenerarReporte;
	}

	public void setBtnGenerarReporte(JButton btnGenerarReporte) {
		this.btnGenerarReporte = btnGenerarReporte;
	}

	public JComboBox getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}

	public JDateChooser getDcDesde() {
		return dcDesde;
	}

	public void setDcDesde(JDateChooser dcDesde) {
		this.dcDesde = dcDesde;
	}

	public JDateChooser getDcHasta() {
		return dcHasta;
	}

	public void setDcHasta(JDateChooser dcHasta) {
		this.dcHasta = dcHasta;
	}

}