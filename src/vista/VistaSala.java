package vista;

import controlador.ControladorSala;

@SuppressWarnings("serial")
public class VistaSala extends VistaABM {

	private ControladorSala cSala;

	public VistaSala(ControladorSala cSala) {
		this.setcSala(cSala);
		setTitle("Salas");
		this.getBtnAgregar().addActionListener(this.getcSala());
		this.getBtnModificar().addActionListener(this.getcSala());
		this.getBtnEliminar().addActionListener(this.getcSala());
		this.addWindowListener(this.getcSala());
		this.getTxtBuscar().setVisible(false);
		this.getBtnEliminar().setVisible(false);
		this.getBtnModificar().setVisible(false);

	}

	public ControladorSala getcSala() {
		return cSala;
	}

	public void setcSala(ControladorSala cSala) {
		this.cSala = cSala;
	}

}
