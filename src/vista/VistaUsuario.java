package vista;

import java.awt.Font;

import javax.swing.table.DefaultTableModel;

import controlador.ControladorUsuario;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeListener;

import java.awt.Color;

@SuppressWarnings("serial")
public class VistaUsuario extends VistaABM {

	private ControladorUsuario cUsuario;
	

	public VistaUsuario(ControladorUsuario cUsuario) {

		this.setcUsuario(cUsuario);
		setTitle("Usuarios");
		this.addWindowListener(this.getcUsuario());
		this.getTxtBuscar().setVisible(true);
		this.getBtnAgregar().addActionListener(this.getcUsuario());
		this.getBtnModificar().addActionListener(this.getcUsuario());
		this.getBtnEliminar().addActionListener(this.getcUsuario());
		this.getTxtBuscar().addKeyListener(this.getcUsuario());

		this.getTable().setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Id_usuario", "DNI", "Nombre", "Apellido", "Nombre de usuario" }));

		JLabel lblBuscarUsuarios = new JLabel("Buscar usuarios");
		lblBuscarUsuarios.setFont(new Font("Arial", Font.PLAIN, 15));
		getPanelBuscar().add(lblBuscarUsuarios);
		
		

	}

	public ControladorUsuario getcUsuario() {
		return cUsuario;
	}

	public void setcUsuario(ControladorUsuario cUsuario) {
		this.cUsuario = cUsuario;
	}


	

}
