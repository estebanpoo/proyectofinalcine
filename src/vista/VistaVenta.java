package vista;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import controlador.ControladorVenta;
import java.awt.Color;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class VistaVenta extends JDialog {

	private ControladorVenta cVenta;
	private JButton btnPelicula;
	private JButton btnPelicula2;
	private JButton btnPelicula3;
	private JButton btnPelicula4;
	private JButton btnPelicula5;
	private JButton btnPelicula6;

	public VistaVenta(ControladorVenta cVenta) {
		this.setcVenta(cVenta);
		this.setIconImage(
				Toolkit.getDefaultToolkit().getImage(VistaVenta.class.getResource("/imagenesPeliculas/pochoclos.jpg")));
		this.setTitle("Venta - Elegir pel�cula");
		this.setModal(true);
		this.addWindowListener(this.getcVenta());
		setBounds(100, 100, 1200, 800);
		setLocationRelativeTo(null);
		setResizable(false);

		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(100, 100, 400, 300);
		setResizable(false);
		getContentPane().add(panel);
		panel.setLayout(null);

		btnPelicula = new JButton("Pelicula 1");
		btnPelicula.setBounds(187, 100, 200, 250);
		btnPelicula.addActionListener(this.getcVenta());
		panel.add(btnPelicula);

		btnPelicula2 = new JButton("Pelicula 2");
		btnPelicula2.setBounds(487, 100, 200, 250);
		btnPelicula2.addActionListener(this.getcVenta());
		panel.add(btnPelicula2);

		btnPelicula3 = new JButton("Pelicula 3");
		btnPelicula3.setBounds(787, 100, 200, 250);
		btnPelicula3.addActionListener(this.getcVenta());
		panel.add(btnPelicula3);

		btnPelicula4 = new JButton("Pelicula 4");
		btnPelicula4.setBounds(187, 376, 200, 250);
		btnPelicula4.addActionListener(this.getcVenta());
		panel.add(btnPelicula4);

		btnPelicula5 = new JButton("Pelicula 5");
		btnPelicula5.setBounds(487, 376, 200, 250);
		btnPelicula5.addActionListener(this.getcVenta());
		panel.add(btnPelicula5);

		btnPelicula6 = new JButton("Pelicula 6");
		btnPelicula6.setBounds(787, 376, 200, 250);
		btnPelicula6.addActionListener(this.getcVenta());
		panel.add(btnPelicula6);

	}

	public JButton getBtnPelicula() {
		return btnPelicula;
	}

	public void setBtnPelicula(JButton btnPelicula) {
		this.btnPelicula = btnPelicula;
	}

	public JButton getBtnPelicula2() {
		return btnPelicula2;
	}

	public void setBtnPelicula2(JButton btnPelicula2) {
		this.btnPelicula2 = btnPelicula2;
	}

	public JButton getBtnPelicula3() {
		return btnPelicula3;
	}

	public void setBtnPelicula3(JButton btnPelicula3) {
		this.btnPelicula3 = btnPelicula3;
	}

	public JButton getBtnPelicula4() {
		return btnPelicula4;
	}

	public void setBtnPelicula4(JButton btnPelicula4) {
		this.btnPelicula4 = btnPelicula4;
	}

	public JButton getBtnPelicula5() {
		return btnPelicula5;
	}

	public void setBtnPelicula5(JButton btnPelicula5) {
		this.btnPelicula5 = btnPelicula5;
	}

	public JButton getBtnPelicula6() {
		return btnPelicula6;
	}

	public void setBtnPelicula6(JButton btnPelicula6) {
		this.btnPelicula6 = btnPelicula6;
	}

	public ControladorVenta getcVenta() {
		return cVenta;
	}

	public void setcVenta(ControladorVenta cVenta) {
		this.cVenta = cVenta;
	}

}
